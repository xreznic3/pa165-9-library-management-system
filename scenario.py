import time
import random
from locust import HttpUser, task, between
from requests import Response

class BorrowUser(HttpUser):
    wait_time = between(1, 3)
    usr_id = 0
    @task(3)
    def get_all_borrows(self):
        self.client.get("http://localhost:8081/api/borrow/all",headers={"Authorization": f"Bearer {self.bearer}"})

    @task(1)
    def get_all_books(self):
        self.client.get("http://localhost:8082/books",headers={"Authorization": f"Bearer {self.bearer}"})  

    def generate_user(self):
        user_data = {
            "userName": "GFFDASFOASMF234",
            "role": 1,
            "password": "1234",
            "firstName": "Franta",
            "middleName": "Antonin",
            "lastName": "Novák"
        }
        return user_data 

    @task(0)
    def create_user_name(self):
        response = self.client.post("http://localhost:8080/users", json=self.generate_user(),headers={"Authorization": f"Bearer {self.bearer}"})
        self.usr_id = response.json().get("id")

    def generate_new_borrow_data(self):
        borrow_data = {
            "userId": self.usr_id,
            "bookId": 1,
            "price": 44.2,
            "returnLimitDays": 12,
            "finePerDay": 10
        }
        return borrow_data

    @task(2)
    def create_borrow_and_return(self):
        borrow_data = self.generate_new_borrow_data()
        response = self.client.post("http://localhost:8081/api/borrow", json=borrow_data,headers={"Authorization": f"Bearer {self.bearer}"})
        borrow_id = response.json().get("id")
        self.client.post(f"http://localhost:8081/api/borrow/return/{borrow_id}",headers={"Authorization": f"Bearer {self.bearer}"})

    @task(4)
    def get_overdue_borrows(self):
        self.client.get("http://localhost:8081/api/borrow/overdue",headers={"Authorization": f"Bearer {self.bearer}"})

    


    def on_start(self):
        f = open("./bearer.txt", "r")
        if not f:
            raise BaseException("File bearer.txt not found")
        self.bearer = f.read()
        if not self.bearer or self.bearer == '':
            raise BaseException("No bearer token found in bearer.txt")
    
