package library.user.service;

import library.user.data.mapping.UserMapping;
import library.user.data.model.User;
import library.user.data.repository.UserRepository;
import library.user.dto.AddUserDTO;
import library.user.dto.UpdateUserDTO;
import library.user.dto.UserRole;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class UserManagementServiceImplTests {

    private UserManagementServiceImpl userManagementService;
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        userRepository = Mockito.mock(UserRepository.class);
        userManagementService = new UserManagementServiceImpl(userRepository);
    }

    @Test
    void getUsersTest() {
        when(userRepository.findAll()).thenReturn(List.of(getUser()));
        Collection<User> found = userManagementService.getUsers();
        User user = found.iterator().next();
        assertEquals(1, found.size());
        assertUser(user);
    }

    @Test
    void getUsersTestWithParameters() {
        when(userRepository.filterUsers(any(), any(), any(), any())).thenReturn(List.of(getUser()));
        Collection<User> found = userManagementService.getUsers(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
        User user = found.iterator().next();
        assertEquals(1, found.size());
        assertUser(user);
    }

    @Test
    void addUserTest() {
        AddUserDTO addUser= new AddUserDTO();
        addUser.setEmail(getUser().getEmail());
        addUser.setUserName(getUser().getUserName());
        addUser.setFirstName(getUser().getFirstName());
        addUser.setRole(getUser().getRole());
        addUser.setLastName(getUser().getLastName());
        addUser.setMiddleName(getUser().getMiddleName());
        addUser.setPassword(getUser().getPassword());
        when(userRepository.save(UserMapping.getUserEntity(addUser))).thenReturn(getUser());

        var user= userManagementService.addUser(addUser);

        assertEquals("email",user.getEmail());
        assertEquals("username",user.getUserName());
        assertEquals("firstname",user.getFirstName());
        assertEquals(UserRole.LIBRARIAN,user.getRole());
        assertEquals("lastname",user.getLastName());
        assertEquals("middlename",user.getMiddleName());
        assertEquals("password",user.getPassword());
    }


    @Test
    void getUsersByRoleTest() {
        when(userRepository.findUsersByRole(UserRole.LIBRARIAN)).thenReturn(List.of(getUser()));
        Collection<User> found = userManagementService.getUsersByRole(UserRole.LIBRARIAN);
        User user = found.iterator().next();
        assertEquals(1, found.size());
        assertUser(user);
    }

    @Test
    void updateUserTest(){
        when(userRepository.findByUsername(getUser().getUserName())).thenReturn(Optional.of(getUser()));

        UpdateUserDTO updateUser= new UpdateUserDTO();
        updateUser.setEmail("b");
        updateUser.setUserName(getUser().getUserName());
        updateUser.setFirstName(getUser().getFirstName());
        updateUser.setRole(getUser().getRole());
        updateUser.setLastName(getUser().getLastName());
        updateUser.setMiddleName(getUser().getMiddleName());
        updateUser.setPassword(getUser().getPassword());
        when(userRepository.save(getUser())).thenReturn(getUser());

        userManagementService.updateUser(updateUser);
        User user = userManagementService.getUser(updateUser.getUserName());

        assertEquals("b",user.getEmail());
        assertEquals("username",user.getUserName());
        assertEquals("firstname",user.getFirstName());
        assertEquals(UserRole.LIBRARIAN,user.getRole());
        assertEquals("lastname",user.getLastName());
        assertEquals("middlename",user.getMiddleName());
        assertEquals("password",user.getPassword());

    }


    @Test
    void getUserTest() {
        when(userRepository.findByUsername("username")).thenReturn(Optional.of(getUser()));
        User user = userManagementService.getUser("username");
        assertUser(user);
    }

    private User getUser() {
        User user = new User();
        user.setId(1L);
        user.setEmail("email");
        user.setUserName("username");
        user.setFirstName("firstname");
        user.setRole(UserRole.LIBRARIAN);
        user.setLastName("lastname");
        user.setMiddleName("middlename");
        user.setPassword("password");
        return user;
    }

    private AddUserDTO getUserDTO() {
        AddUserDTO addUserDTO = new AddUserDTO();
        addUserDTO.setEmail("email");
        addUserDTO.setUserName("username");
        addUserDTO.setFirstName("firstname");
        addUserDTO.setRole(UserRole.LIBRARIAN);
        addUserDTO.setLastName("lastname");
        addUserDTO.setMiddleName("middlename");
        addUserDTO.setPassword("password");
        return addUserDTO;
    }

    private void assertUser(User user) {
        assertNotNull(user);
        assertEquals(1L, user.getId());
        assertEquals("email", user.getEmail());
        assertEquals("username", user.getUserName());
        assertEquals("firstname", user.getFirstName());
        assertEquals(UserRole.LIBRARIAN, user.getRole());
        assertEquals("lastname", user.getLastName());
        assertEquals("middlename", user.getMiddleName());
        assertEquals("password", user.getPassword());
    }
}
