package library.user.controller;

import library.user.facade.UserFacade;
import library.user.dto.AddUserDTO;
import library.user.dto.UpdateUserDTO;
import library.user.dto.UserDetailDTO;
import library.user.dto.UserListEntityDTO;
import library.user.rest.UserController;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collection;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserManagementControllerTests {

    @Test
    void getUsersTest() {
        UserFacade userFacade = Mockito.mock(UserFacade.class);
        UserController userController = new UserController(userFacade);
        when(userFacade.getUsers()).thenReturn(ResponseEntity.ok().build());

        ResponseEntity<Collection<UserListEntityDTO>> response = userController.getUsers(Optional.empty());

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getUserTest() {
        UserFacade userFacade = Mockito.mock(UserFacade.class);
        UserController userController = new UserController(userFacade);
        when(userFacade.getUser(anyString())).thenReturn(ResponseEntity.ok().build());

        ResponseEntity<UserDetailDTO> response = userController.getUser("username");

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void addUserTest() {

        UserFacade userFacade = Mockito.mock(UserFacade.class);
        UserController userController = new UserController(userFacade);
        AddUserDTO addUserDTO = new AddUserDTO();
        when(userFacade.addUser(addUserDTO)).thenReturn(ResponseEntity.status(HttpStatus.CREATED).build());

        ResponseEntity<UserDetailDTO> response = userController.addUser(addUserDTO);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    void editUserTest() {

        UserFacade userFacade = Mockito.mock(UserFacade.class);
        UserController userController = new UserController(userFacade);
        UpdateUserDTO updateUserDTO = new UpdateUserDTO();

        userController.editUser(updateUserDTO);

        verify(userFacade).updateUser(updateUserDTO);
    }

    @Test
    void deleteUserTest() {

        UserFacade userFacade = Mockito.mock(UserFacade.class);
        UserController userController = new UserController(userFacade);
        String userName = "username";

        Mockito.doNothing().when(userFacade).deleteUser(userName);

        userController.deleteUser(userName);

    }

    @Test
    void filterUsersTest() {
        UserFacade userFacade = Mockito.mock(UserFacade.class);
        UserController userController = new UserController(userFacade);
        when(userFacade.getUsers(any(), any(), any(), any())).thenReturn(ResponseEntity.ok().build());

        ResponseEntity<Collection<UserListEntityDTO>> response = userController.filterUsers(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());


        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}
