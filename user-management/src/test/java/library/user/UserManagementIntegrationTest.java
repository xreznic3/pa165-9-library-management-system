package library.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import library.user.data.model.User;
import library.user.data.model.auth.Scopes;
import library.user.data.repository.UserRepository;
import library.user.dto.AddUserDTO;
import library.user.dto.UpdateUserDTO;
import library.user.dto.UserRole;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@ActiveProfiles("test")
class UserManagementIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    public void setup() {
        userRepository.deleteAll();
    }

    private RequestPostProcessor mockLibrarian() {
        return SecurityMockMvcRequestPostProcessors.jwt().authorities(new SimpleGrantedAuthority(Scopes.LIBRARIAN));
    }


    @Test
    public void getUsersTest() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/users")
                        .with(mockLibrarian()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.length()").value(0));
    }

    @Test
    public void getUserByNameTest() throws Exception {

        User user = new User();
        user.setUserName("Vašek");
        user.setEmail("example@examle.com");
        user.setFirstName("Václav");
        user.setLastName("Novák");
        user.setRole(UserRole.MEMBER);
        user.setPassword("123456");
        user.setMiddleName("");


        userRepository.save(user);


        mockMvc.perform(MockMvcRequestBuilders.get("/users/byUsername")
                        .with(mockLibrarian())
                        .param("username", "Vašek"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userName").value("Vašek"))
                .andExpect(jsonPath("$.emailAddress").value("example@examle.com"));
    }

    @Test
    public void addUserTest() throws Exception {

        AddUserDTO userDTO = new AddUserDTO();
        userDTO.setUserName("JohnDoe");
        userDTO.setEmail("john@example.com");
        userDTO.setFirstName("John");
        userDTO.setLastName("Doe");
        userDTO.setRole(UserRole.MEMBER);
        userDTO.setPassword("password");


        String userJson = objectMapper.writeValueAsString(userDTO);


        mockMvc.perform(MockMvcRequestBuilders.post("/users")
                        .contentType("application/json")
                        .content(userJson)
                        .with(mockLibrarian()))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.userName").value("JohnDoe"))
                .andExpect(jsonPath("$.emailAddress").value("john@example.com"));


    }

    @Test
    public void editUserTest() throws Exception {
        User user = new User();
        user.setUserName("Vašek");
        user.setEmail("example@examle.com");
        user.setFirstName("Václav");
        user.setLastName("Novák");
        user.setRole(UserRole.MEMBER);
        user.setPassword("123456");
        user.setMiddleName("");


        userRepository.save(user);


        UpdateUserDTO userDTO = new UpdateUserDTO();
        userDTO.setUserName("Vašek");
        userDTO.setFirstName("Václav");
        userDTO.setLastName("Novák Jr.");
        userDTO.setRole(UserRole.MEMBER);
        userDTO.setPassword("newPassword");

        String userJson = objectMapper.writeValueAsString(userDTO);


        mockMvc.perform(MockMvcRequestBuilders.put("/users")
                        .with(mockLibrarian())
                        .contentType("application/json")
                        .content(userJson))
                .andExpect(status().isOk());


    }

    @Test
    public void deleteUserTest() throws Exception {
        // Create a sample user
        User user = new User();
        user.setUserName("ToDelete");
        user.setEmail("delete@example.com");
        user.setFirstName("Delete");
        user.setLastName("Me");
        user.setRole(UserRole.MEMBER);
        user.setPassword("password");


        userRepository.save(user);


        mockMvc.perform(MockMvcRequestBuilders.delete("/users")
                        .with(mockLibrarian())
                        .param("userName", "ToDelete"))
                .andExpect(status().isOk());


    }




}
