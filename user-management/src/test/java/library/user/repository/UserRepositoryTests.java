package library.user.repository;

import library.user.data.model.User;
import library.user.data.repository.UserRepository;
import library.user.dto.UserRole;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@DataJpaTest
public class UserRepositoryTests {

    @MockBean
    private UserRepository userRepository;


    @Test
    public void testFindByUsername() {
        String username = "testUser";
        User user = new User();
        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));

        Optional<User> result = userRepository.findByUsername(username);

        assertTrue(result.isPresent());
        assertEquals(user, result.get());
    }

    @Test
    public void testFilterUsers() {
        Optional<String> username = Optional.of("testUser");
        Optional<UserRole> role = Optional.of(UserRole.MEMBER);
        Optional<String> firstname = Optional.of("John");
        Optional<String> lastname = Optional.empty();
        List<User> userList = Arrays.asList(
                new User(),
                new User()
        );
        when(userRepository.filterUsers(username, role, firstname, lastname)).thenReturn(userList);

        List<User> result = userRepository.filterUsers(username, role, firstname, lastname);

        assertNotNull(result);
        assertEquals(2, result.size());
    }

    @Test
    public void testFindUsersByRole() {
        UserRole role = UserRole.MEMBER;
        List<User> userList = Arrays.asList(
                new User(),
                new User()
        );
        when(userRepository.findUsersByRole(role)).thenReturn(userList);

        List<User> result = userRepository.findUsersByRole(role);

        assertNotNull(result);
        assertEquals(2, result.size());
        }

}
