package library.user.facade;

import library.user.dto.*;
import library.user.data.mapping.UserMapping;
import library.user.data.model.User;
import library.user.service.UserManagementService;
import library.user.service.UserManagementServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UserFacadeTests {

    private UserFacade userFacade;

    private UserManagementService userService;


    @BeforeEach
    void setUp() {
        userService = Mockito.mock(UserManagementServiceImpl.class);
        userFacade = new UserFacade(userService);
    }

    @Test
    public void testGetUsers() {
        List<User> userList = Arrays.asList();
        when(userService.getUsers()).thenReturn(userList);


        ResponseEntity<Collection<UserListEntityDTO>> responseEntity = userFacade.getUsers();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(userList,responseEntity.getBody());
    }

    @Test
    public void testAddUser() {
        AddUserDTO addUserDTO = new AddUserDTO();
        User user = new User();
        user.setId(1L);
        user.setUserName("testUser");
        user.setRole(UserRole.MEMBER);
        user.setPassword("password");
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setEmail("john.doe@example.com");
        when(userService.addUser(addUserDTO)).thenReturn(user);

        ResponseEntity<UserDetailDTO> responseEntity = userFacade.addUser(addUserDTO);


        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());

    }

    @Test
    public void testGetUsersByRole() {
        UserRole role = UserRole.MEMBER;
        List<User> userList = List.of();
        when(userService.getUsersByRole(role)).thenReturn(userList);

        ResponseEntity<Collection<UserListEntityDTO>> responseEntity = userFacade.getUsersByRole(role);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void testUpdateUser() {
        UpdateUserDTO updateUserDTO = new UpdateUserDTO();

        userFacade.updateUser(updateUserDTO);

        verify(userService, times(1)).updateUser(updateUserDTO);
    }

    @Test
    public void testGetUser() {
        String username = "testUser";
        User user = new User();
        user.setId(1L);
        user.setUserName("testUser");
        user.setRole(UserRole.MEMBER);
        user.setPassword("password");
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setEmail("john.doe@example.com");
        when(userService.getUser(username)).thenReturn(user);


        UserDetailDTO userDetailDTO = UserMapping.getUserDetailDto(user);


        ResponseEntity<UserDetailDTO> responseEntity = userFacade.getUser(username);


        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(userDetailDTO, responseEntity.getBody());
    }

    @Test
    public void testDeleteUser() {
        String username = "testUser";
        userFacade.deleteUser(username);

        verify(userService, times(1)).deleteUser(username);
    }
}
