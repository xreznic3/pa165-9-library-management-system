package library.user.facade;


import library.user.dto.*;
import library.user.data.mapping.UserMapping;
import library.user.service.UserManagementService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
public class UserFacade {

    final UserManagementService userService;

    public UserFacade(UserManagementService userService) {
        this.userService = userService;
    }

    public ResponseEntity<Collection<UserListEntityDTO>> getUsers() {
        return new ResponseEntity<>(userService.getUsers().stream().map(UserMapping::getUserListEntityDto).toList(), HttpStatus.OK);
    }

    public ResponseEntity<Collection<UserListEntityDTO>> getUsers(Optional<String> username, Optional<UserRole> role, Optional<String> firstname, Optional<String> lastname) {
        return new ResponseEntity<>(userService.getUsers(username, role, firstname, lastname)
                .stream()
                .map(UserMapping::getUserListEntityDto).toList(), HttpStatus.OK);
    }

    public ResponseEntity<UserDetailDTO> addUser(AddUserDTO user) {
        return new ResponseEntity<>(UserMapping.getUserDetailDto(userService.addUser(user)), HttpStatus.CREATED);
    }

    public ResponseEntity<Collection<UserListEntityDTO>> getUsersByRole(UserRole userRole) {
        return new ResponseEntity<>(userService.getUsersByRole(userRole).stream().map(UserMapping::getUserListEntityDto).toList(), HttpStatus.OK);
    }

    public void updateUser(UpdateUserDTO user) {
        userService.updateUser(user);
    }

    @Transactional(readOnly = true)
    public ResponseEntity<UserDetailDTO> getUser(String username) {
        return new ResponseEntity<>(UserMapping.getUserDetailDto(userService.getUser(username)), HttpStatus.OK);
    }

    public void deleteUser(String userName) {
        userService.deleteUser(userName);
    }
}
