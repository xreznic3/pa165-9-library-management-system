package library.user.dto;

public enum UserRole {
    MEMBER,
    LIBRARIAN
}
