package library.user.dto;

import io.swagger.v3.oas.annotations.media.Schema;


public class UserListEntityDTO {
    private String firstName;
    private String middleName;
    private String lastName;
    private UserRole role;
    private String userName;

    @Schema(
            name = "firstName",
            example = "John",
            description = "User first name",
            requiredMode = Schema.RequiredMode.REQUIRED
    )
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Schema(
            name = "middleName",
            example = "Adam",
            description = "User middle name(not required)",
            requiredMode = Schema.RequiredMode.NOT_REQUIRED
    )
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @Schema(
            name = "lastName",
            example = "Wick",
            description = "User last name",
            requiredMode = Schema.RequiredMode.REQUIRED
    )
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Schema(
            name = "role",
            example = "MEMBER",
            description = "User role",
            requiredMode = Schema.RequiredMode.REQUIRED
    )
    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Schema(
            name = "userName",
            example = "book_lover22",
            description = "Unique user name",
            requiredMode = Schema.RequiredMode.REQUIRED
    )
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
