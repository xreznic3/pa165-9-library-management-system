package library.user.dto;

import io.swagger.v3.oas.annotations.media.Schema;


public class UpdateUserDTO {
    private String userName;
    private UserRole role;
    private String password;
    private String firstName;
    private String middleName;
    private String lastName;
    private String email;

    @Schema(
            name = "userName",
            example = "book_lover22",
            description = "Unique user name",
            requiredMode = Schema.RequiredMode.REQUIRED
    )
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Schema(
            name = "role",
            example = "MEMBER",
            description = "User role",
            requiredMode = Schema.RequiredMode.REQUIRED
    )
    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Schema(
            name = "password",
            example = "111fca2d52def4c33f4d8f1be7e74d14b65d365e5ddb91610c3c0dbecc192073b0b0df28213e3828cc0321f6286baf94449a4f8803203be3293595f4d67ff7e2",
            description = "Encrypted user password",
            requiredMode = Schema.RequiredMode.REQUIRED
    )
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Schema(
            name = "firstName",
            example = "John",
            description = "User first name",
            requiredMode = Schema.RequiredMode.REQUIRED
    )
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Schema(
            name = "middleName",
            example = "Adam",
            description = "User middle name(not required)",
            requiredMode = Schema.RequiredMode.NOT_REQUIRED
    )
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @Schema(
            name = "lastName",
            example = "Wick",
            description = "User last name",
            requiredMode = Schema.RequiredMode.REQUIRED
    )
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Schema(
            name = "email",
            example = "book_lover@gmail.com",
            description = "User email address",
            requiredMode = Schema.RequiredMode.NOT_REQUIRED
    )
    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
