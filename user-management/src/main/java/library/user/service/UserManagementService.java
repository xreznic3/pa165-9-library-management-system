package library.user.service;



import library.user.data.model.User;
import library.user.dto.AddUserDTO;
import library.user.dto.UpdateUserDTO;
import library.user.dto.UserRole;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.Optional;

public interface UserManagementService {
    /**
     * Get all users that are stored in database.
     * @return Collection of the users.
     */
    Collection<User> getUsers();

    /**
     * Get users based on optional filter parameters given.
     * @see User
     * @param username Filter users by username(if username filter is used,
     *                 only collection with maximum length 1 is always returned).
     * @param role Filter users by role(Manager or Buyer).
     * @param firstname Filter users by first name.
     * @param lastname Filter users by last name.
     * @return Collection of Users according to filter parameters given.
     */
    Collection<User> getUsers(Optional<String> username, Optional<UserRole> role, Optional<String> firstname, Optional<String> lastname);

    /**
     * Add user to database.
     * @see User
     * @param user User parameters that will be stored into database.
     * @return Stored user entity with ID.
     * @throws ResponseStatusException if user cannot be created
     */
    User addUser(AddUserDTO user);

    /**
     * Get users in the role given.
     * @see User
     * @see UserRole
     * @param userRole user role.
     * @return collection of the users in user role given.
     */
    Collection<User> getUsersByRole(UserRole userRole);

    /**
     * Update user in database.
     * @see UpdateUserDTO
     * @param user Updated parameters of the user.
     * @throws ResponseStatusException if user to be updated was not found.
     */
    void updateUser(UpdateUserDTO user);

    /**
     * Get specific user by username given.
     * @see User
     * @param username
     * @return User with username given in parameter.
     * @throws ResponseStatusException if user with username given does not exist.
     */
    User getUser(String username);

    /**
     * Delete user by username.
     * @param userName
     * @throws ResponseStatusException if user with username given does not exist.
     */
    void deleteUser(String userName);
}
