package library.user.service;

import library.user.data.mapping.UserMapping;
import library.user.data.model.User;
import library.user.data.repository.UserRepository;

import library.user.dto.AddUserDTO;
import library.user.dto.UpdateUserDTO;
import library.user.dto.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.Optional;

@Service
public class UserManagementServiceImpl implements UserManagementService {

    private final UserRepository userRepository;

    @Autowired
    public UserManagementServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional(readOnly = true)
    public Collection<User> getUsers() {
        return userRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Collection<User> getUsers(Optional<String> username, Optional<UserRole> role, Optional<String> firstname, Optional<String> lastname) {
        return userRepository.filterUsers(username, role, firstname, lastname);
    }

    @Transactional
    public User addUser(AddUserDTO user) {
        User u = UserMapping.getUserEntity(user);

        String errorMsg = checkUserParams(u, true);

        if (errorMsg != "") {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot create user because: " + errorMsg);
        }

        userRepository.save(u);
        return u;
    }

    @Transactional(readOnly = true)
    public Collection<User> getUsersByRole(UserRole userRole) {
        return userRepository.findUsersByRole(userRole);
    }

    @Transactional
    public void updateUser(UpdateUserDTO user) {
        User u = getUserOrThrow(user.getUserName());
        u.setFirstName(user.getFirstName());
        u.setLastName(user.getLastName());
        u.setMiddleName(user.getMiddleName());
        u.setRole(user.getRole());
        u.setEmail(user.getEmail());
        u.setPassword(user.getPassword());

        String errorMsg = checkUserParams(u, false);

        if (errorMsg != "") {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot update user because: " + errorMsg);
        }

        userRepository.save(u);
    }

    @Transactional(readOnly = true)
    public User getUser(String username) {
        return getUserOrThrow(username);
    }

    @Transactional
    public void deleteUser(String userName) {
        User user = getUserOrThrow(userName);
        userRepository.delete(user);
    }

    private User getUserOrThrow(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with username " + username + " does not exist."));
    }

    private String checkUserParams(User user, boolean unique) {
        String errorMsg = "";

        if (user.getUserName() == null || user.getUserName() == "") {
            errorMsg = "Username cannot be empty or missing. ";
        }
        if (unique && userRepository.findByUsername(user.getUserName()).isPresent()) {
            errorMsg += "User with username " + user.getUserName() + " already exists. ";
        }
        if (user.getRole() == null) {
            errorMsg += "User role cannot be empty. ";
        }
        if (user.getFirstName() == null || user.getFirstName() == "") {
            errorMsg += "First name cannot be empty or missing. ";
        }
        if (user.getLastName() == null || user.getLastName() == "") {
            errorMsg += "Last name cannot be empty or missing. ";
        }
        if (user.getPassword() == null || user.getPassword() == "") {
            errorMsg += "Password cannot be empty or missing. ";
        }

        return errorMsg;
    }
}
