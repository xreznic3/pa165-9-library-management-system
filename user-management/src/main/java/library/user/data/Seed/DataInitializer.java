package library.user.data.Seed;

import library.user.data.model.User;
import library.user.data.repository.UserRepository;
import library.user.dto.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static library.user.dto.UserRole.LIBRARIAN;
import static library.user.dto.UserRole.MEMBER;


@Component
public class DataInitializer implements ApplicationRunner {

    @Autowired
    private UserRepository userRepository;
    DataGenerator dataGenerator = new DataGenerator();
    static Random random = new Random();


    public static String generatePassword(int length) {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+";
        Random random = new Random();
        StringBuilder password = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(characters.length());
            password.append(characters.charAt(index));
        }

        return password.toString();
    }


    public void seedUsers(int generatedUsers) {
        DataGenerator dataGenerator = new DataGenerator();
        UserRole[] userRoles = {MEMBER, LIBRARIAN};
        for (int i = 0; i < generatedUsers; i++) {
            User user = new User();
            user.setUserName(dataGenerator.usernames[i]);
            user.setEmail(dataGenerator.getRandomMail());
            user.setFirstName(dataGenerator.getRandomFullName().split(" ")[0]);
            user.setLastName(dataGenerator.getRandomFullName().split(" ")[1]);
            user.setMiddleName(dataGenerator.getRandomFullName().split(" ")[0]);
            user.setRole(userRoles[random.nextInt(2)]);
            user.setPassword(generatePassword(random.nextInt(5)+10));
            userRepository.save(user);
        }
    }
    @Override
    public void run(ApplicationArguments args) throws Exception {
        seedUsers(50);
    }
}
