package library.user.data.mapping;

import library.user.data.model.User;
import library.user.dto.AddUserDTO;
import library.user.dto.UserDetailDTO;
import library.user.dto.UserListEntityDTO;

/**
 * Class to map user DTOs to User entity and the opposite way
 */
public class UserMapping {
    /**
     * Map <code>User</code> to <code>UserListEntityDTO</code>
     * @see User
     * @see UserListEntityDTO
     * @param user
     * @return <code>UserListEntityDTO</code> with parameters based on user given.
     */
    public static UserListEntityDTO getUserListEntityDto(User user) {
        UserListEntityDTO dto = new UserListEntityDTO();
        dto.setUserName(user.getUserName());
        dto.setRole(user.getRole());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setMiddleName(user.getMiddleName());

        return dto;
    }

    /**
     * Map <code>User</code> to <code>UserDetailDTO</code>
     * @see User
     * @see UserDetailDTO
     * @param user
     * @return <code>UserDetailDTO</code> with parameters based on user given.
     */
    public static UserDetailDTO getUserDetailDto(User user) {
        UserDetailDTO dto = new UserDetailDTO();
        dto.setUserID(user.getId());
        dto.setUserName(user.getUserName());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setRole(user.getRole());
        dto.setMiddleName(user.getMiddleName());
        dto.setFirstName(user.getFirstName());
        dto.setEmailAddress(user.getEmail());

        return dto;
    }

    /**
     * Map <code>AddUserDTO</code> to <code>User</code>
     * @see User
     * @see AddUserDTO
     * @param user
     * @return <code>User</code> with parameters based on <code>AddUserDTO</code> given.
     */
    public static User getUserEntity(AddUserDTO user) {
        User u = new User();
        u.setPassword(user.getPassword());
        u.setUserName(user.getUserName());
        u.setRole(user.getRole());
        u.setLastName(user.getLastName());
        u.setMiddleName(user.getMiddleName());
        u.setFirstName(user.getFirstName());
        u.setEmail(user.getEmail());

        return u;
    }
}
