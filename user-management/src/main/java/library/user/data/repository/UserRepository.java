package library.user.data.repository;

import library.user.data.model.User;

import library.user.dto.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u WHERE u.userName = :userName")
    Optional<User> findByUsername(@Param("userName") String userName);

    @Query("SELECT u FROM User u WHERE (:username is null OR :username = u.userName) AND (:role is null OR :role = u.role) AND (:firstname is null OR :firstname = u.firstName) AND (:lastname is null OR :lastname = u.lastName)")
    List<User> filterUsers(@Param("username") Optional<String> username, @Param("role") Optional<UserRole> role, @Param("firstname") Optional<String> firstname, @Param("lastname") Optional<String> lastname);

    @Query("SELECT u FROM User u WHERE :role = u.role")
    List<User> findUsersByRole(UserRole role);
}
