package library.user.rest;

import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import library.user.dto.*;
import library.user.facade.UserFacade;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserFacade userFacade;

    public UserController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @Operation(
            operationId = "getUsers",
            summary = "Gets list of users",
            tags = { "UserController" },
            security = { @SecurityRequirement(name = "Bearer Authentication") },
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = UserListEntityDTO.class))
                    })
            }
    )
    @GetMapping
    public ResponseEntity<Collection<UserListEntityDTO>> getUsers(
            @Parameter(name = "userRole", description = "User role to get users in particular role", required = false, in = ParameterIn.QUERY) @Valid @RequestParam(value = "userRole", required = false) @ApiParam(required = false) Optional<UserRole> userRole
    ) {
        if (userRole.isEmpty()) {
            return userFacade.getUsers();
        }

        return userFacade.getUsersByRole(userRole.get());
    }

    @Operation(
            operationId = "getUser",
            summary = "Gets user by username",
            tags = { "UserController" },
            security = { @SecurityRequirement(name = "Bearer Authentication") },
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = UserDetailDTO.class))
                    }),
                    @ApiResponse(responseCode = "400", description = "User not found")
            }
    )
    @GetMapping("/byUsername")
    public ResponseEntity<UserDetailDTO> getUser(
            @Parameter(name = "username", description = "User unique identifier", required = true, in = ParameterIn.QUERY) @Valid @RequestParam(value = "username", required = true) @ApiParam(required = true) String username
    ) {
        return userFacade.getUser(username);
    }

    @PostMapping
    @Operation(
            operationId = "addUser",
            summary = "Add user specified in the request body",
            tags = { "UserController" },
            security = { @SecurityRequirement(name = "Bearer Authentication") },
            responses = {
                    @ApiResponse(responseCode = "201", description = "Created", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = UserDetailDTO.class)),
                    }),
                    @ApiResponse(responseCode = "400", description = "User parameters are invalid.")
            }
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<UserDetailDTO> addUser(@NotNull @Valid @RequestBody @Schema(implementation = AddUserDTO.class) AddUserDTO user) {
        return userFacade.addUser(user);
    }

    @PutMapping
    @Operation(
            operationId = "editUser",
            summary = "Edit user specified in the request body",
            tags = { "UserController" },
            security = { @SecurityRequirement(name = "Bearer Authentication") },
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "400", description = "User with given username does not exist and cannot be edited.")
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public void editUser(
            @NotNull @Valid @RequestBody @Schema(implementation = UpdateUserDTO.class) UpdateUserDTO user
    ) {
        userFacade.updateUser(user);
    }

    @DeleteMapping
    @Operation(
            operationId = "deleteUser",
            summary = "Delete user by his user name",
            tags = { "UserController" },
            security = { @SecurityRequirement(name = "Bearer Authentication") },
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "400", description = "User with the given username does not exist.")
            }
    )
    public void deleteUser(
            @NotNull @Parameter(name = "userName", description = "Username of the user to be deleted", required = true, in = ParameterIn.QUERY) @Valid @RequestParam(value = "userName", required = true) @PathVariable String userName
    ) {
        userFacade.deleteUser(userName);
    }

    @GetMapping("/search")
    @Operation(
            operationId = "filterUsers",
            summary = "Get users by filter parameters. If parameter has default value or null, parameter is not used in filtering",
            tags = { "UserController" },
            security = { @SecurityRequirement(name = "Bearer Authentication") },
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = UserListEntityDTO.class)),
                    })
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Collection<UserListEntityDTO>> filterUsers(
            @Parameter(name = "Username", description = "Username of the user(if this parameter is set, result will always contain only one user)", allowEmptyValue = true) @RequestParam(value = "userName", required = false) Optional<String> userName,
            @Parameter(name = "role", description = "User role", allowEmptyValue = true) @RequestParam(value = "role", required = false) Optional<UserRole> role,
            @Parameter(name = "First name", description = "User first name", allowEmptyValue = true) @RequestParam(value = "firstName", required = false) Optional<String> firstName,
            @Parameter(name = "Last name", description = "User last name", allowEmptyValue = true) @RequestParam(value = "lastName", required = false) Optional<String> lastName
    ) {
        return userFacade.getUsers(userName, role, firstName, lastName);
    }
}

