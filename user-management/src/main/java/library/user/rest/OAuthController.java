package library.user.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class OAuthController {
    @GetMapping("/")
    public ResponseEntity<String> index(@AuthenticationPrincipal OidcUser user,
                                        @RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient oauth2Client) {

        return new ResponseEntity<>("User with username" + oauth2Client.getPrincipalName() + " was looged in with bearer " +
                oauth2Client.getAccessToken()
                        .getTokenValue(),
                HttpStatus.OK);
    }
}
