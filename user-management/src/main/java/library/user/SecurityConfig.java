package library.user;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import library.user.data.model.auth.Scopes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.oidc.web.logout.OidcClientInitiatedLogoutSuccessHandler;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRequestAttributeHandler;

import java.io.IOException;

@Configuration
@SecurityScheme(
        name = "Bearer Authentication",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        scheme = "bearer"
)
public class SecurityConfig {

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(x -> x
                        .requestMatchers(HttpMethod.PUT).hasAuthority(Scopes.LIBRARIAN)
                        .requestMatchers(HttpMethod.POST).hasAuthority(Scopes.LIBRARIAN)
                        .requestMatchers(HttpMethod.DELETE).hasAuthority(Scopes.LIBRARIAN)
                        .requestMatchers(HttpMethod.GET).authenticated()
                        .anyRequest().permitAll()
                )
                .oauth2Login(x -> x
                        // our custom handler for successful logins
                        .successHandler(authenticationSuccessHandler())
                )
                .logout(x -> x
                        // After we log out, redirect to the root page, by default Spring will send you to /login?logout
                        .logoutSuccessUrl("/oauth/success")
                        // after local logout, do also remote logout at the OIDC Provider too
                        .logoutSuccessHandler(oidcLogoutSuccessHandler())
                )
                .csrf(c -> c
                        //set CSRF token cookie "XSRF-TOKEN" with httpOnly=false that can be read by JavaScript
                        .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                        //replace the default XorCsrfTokenRequestAttributeHandler with one that can use value from the cookie
                        .csrfTokenRequestHandler(new CsrfTokenRequestAttributeHandler())
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()))
        ;
        return http.build();
    }


    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler() {
        return new SavedRequestAwareAuthenticationSuccessHandler() {
            @Override
            public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication auth) throws ServletException, IOException {
                if (auth instanceof OAuth2AuthenticationToken token
                        && token.getPrincipal() instanceof OidcUser user) {
                    System.out.println("********************************************************");
                    System.out.println("* user successfully logged in                          *");
                    System.out.println("********************************************************");
                    System.out.println("user.issuer: "+ user.getIssuer());
                    System.out.println("user.subject: "+ user.getSubject());
                    System.out.println("user.fullName: "+ user.getFullName());
                    System.out.println("user.givenName: "+ user.getGivenName());
                    System.out.println("user.familyName: "+user.getFamilyName());
                    System.out.println("user.gender: "+ user.getGender());
                    System.out.println("user.email: "+ user.getEmail());
                    System.out.println("user.locale: "+ user.getLocale());
                    System.out.println("user.zoneInfo: "+ user.getZoneInfo());
                    System.out.println("user.preferredUsername: "+ user.getPreferredUsername());
                    System.out.println("user.issuedAt: "+ user.getIssuedAt());
                    System.out.println("user.authenticatedAt: "+ user.getAuthenticatedAt());
                    System.out.println("user.claimAsListString(\"eduperson_scoped_affiliation\"): "+ user.getClaimAsStringList("eduperson_scoped_affiliation"));
                    System.out.println("user.attributes.acr: "+ user.<String>getAttribute("acr"));
                    System.out.println("user.attributes: "+ user.getAttributes());
                    System.out.println("user.authorities: "+ user.getAuthorities());
                }
                super.onAuthenticationSuccess(req, res, auth);
            }
        };
    }

    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;

    /**
     * Handler called when local logout successfully completes.
     * It initiates also a complete remote logout at the Authorization Server.
     * @see OidcClientInitiatedLogoutSuccessHandler
     */
    private OidcClientInitiatedLogoutSuccessHandler oidcLogoutSuccessHandler() {
        OidcClientInitiatedLogoutSuccessHandler successHandler =
                new OidcClientInitiatedLogoutSuccessHandler(clientRegistrationRepository);
        successHandler.setPostLogoutRedirectUri("https://localhost:8080/");
        return successHandler;
    }
}