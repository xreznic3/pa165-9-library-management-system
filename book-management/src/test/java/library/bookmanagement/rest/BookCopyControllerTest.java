package library.bookmanagement.rest;

import library.bookmanagement.data.model.BookCopy;
import library.bookmanagement.dto.CreateBookCopyDto;
import library.bookmanagement.dto.UpdateBookCopyDto;
import library.bookmanagement.facade.BookAndBookCopyFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.ArrayList;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BookCopyControllerTest {

    @Mock
    private BookAndBookCopyFacade bookAndBookCopyFacade;

    private BookCopyController bookCopyController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        bookCopyController = new BookCopyController(bookAndBookCopyFacade);
    }

    @Test
    public void getAllBookCopiesTest() {
        Collection<BookCopy> expectedBookCopies = new ArrayList<>();
        expectedBookCopies.add(new BookCopy());
        when(bookAndBookCopyFacade.getAllBookCopies()).thenReturn(expectedBookCopies);

        ResponseEntity<Collection<BookCopy>> response = bookCopyController.getAllBookCopies();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedBookCopies, response.getBody());
    }

    @Test
    public void getAllBookCopiesNoContentTest() {
        when(bookAndBookCopyFacade.getAllBookCopies()).thenReturn(new ArrayList<>());

        ResponseEntity<Collection<BookCopy>> response = bookCopyController.getAllBookCopies();

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    public void getBookCopyByIdTest() {
        Long id = 1L;
        BookCopy expectedBookCopy = new BookCopy();
        when(bookAndBookCopyFacade.getBookCopyById(id)).thenReturn(expectedBookCopy);

        ResponseEntity<BookCopy> response = bookCopyController.getBookCopyById(id);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedBookCopy, response.getBody());
    }

    @Test
    public void getBookCopyByIdNotFoundTest() {
        Long id = 1L;
        when(bookAndBookCopyFacade.getBookCopyById(id)).thenReturn(null);

        ResponseEntity<BookCopy> response = bookCopyController.getBookCopyById(id);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    public void createBookCopyTest() {
        CreateBookCopyDto createBookCopyDto = new CreateBookCopyDto();
        BookCopy expectedBookCopy = new BookCopy();
        when(bookAndBookCopyFacade.createBookCopy(createBookCopyDto)).thenReturn(expectedBookCopy);

        ResponseEntity<BookCopy> response = bookCopyController.createBookCopy(createBookCopyDto);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(expectedBookCopy, response.getBody());
    }

    @Test
    public void updateBookCopyTest() {
        Long id = 1L;
        UpdateBookCopyDto updateBookCopyDto = new UpdateBookCopyDto();
        BookCopy expectedBookCopy = new BookCopy();
        when(bookAndBookCopyFacade.updateBookCopy(id, updateBookCopyDto)).thenReturn(expectedBookCopy);

        ResponseEntity<BookCopy> response = bookCopyController.updateBookCopy(id, updateBookCopyDto);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedBookCopy, response.getBody());
    }

    @Test
    public void deleteBookCopyTest() {
        Long id = 1L;

        ResponseEntity<Void> response = bookCopyController.deleteBookCopy(id);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(bookAndBookCopyFacade).deleteBookCopy(id);
    }
}
