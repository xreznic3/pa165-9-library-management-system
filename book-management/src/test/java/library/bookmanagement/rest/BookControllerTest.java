package library.bookmanagement.rest;

import library.bookmanagement.data.model.Book;
import library.bookmanagement.dto.CreateBookDto;
import library.bookmanagement.dto.UpdateBookDto;
import library.bookmanagement.facade.BookAndBookCopyFacade;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.ArrayList;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BookControllerTest {

    @Mock
    private BookAndBookCopyFacade bookAndBookCopyFacade;

    private BookController bookController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        bookController = new BookController(bookAndBookCopyFacade);
    }

    @Test
    public void getAllBooksTest() {

        Collection<Book> expectedBooks = new ArrayList<>();
        expectedBooks.add(new Book());
        when(bookAndBookCopyFacade.getAllBooks()).thenReturn(expectedBooks);


        ResponseEntity<Collection<Book>> response = bookController.getAllBooks();


        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedBooks, response.getBody());
    }

    @Test
    public void getBookByIdTest() {
        Long id = 1L;
        Book expectedBook = new Book();
        when(bookAndBookCopyFacade.getBookById(id)).thenReturn(expectedBook);

        ResponseEntity<Book> response = bookController.getBookById(id);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedBook, response.getBody());
    }

    @Test
    public void getBookByIdNotFoundTest() {
        Long id = 1L;
        when(bookAndBookCopyFacade.getBookById(id)).thenReturn(null);

        ResponseEntity<Book> response = bookController.getBookById(id);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void createBookTest() {
        CreateBookDto createBookDto = new CreateBookDto();
        Book expectedBook = new Book();
        when(bookAndBookCopyFacade.createBook(createBookDto)).thenReturn(expectedBook);

        ResponseEntity<Book> response = bookController.createBook(createBookDto);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(expectedBook, response.getBody());
    }

    @Test
    public void updateBookTest() {
        Long id = 1L;
        UpdateBookDto updateBookDto = new UpdateBookDto();
        Book expectedBook = new Book();
        when(bookAndBookCopyFacade.updateBook(id, updateBookDto)).thenReturn(expectedBook);

        ResponseEntity<Book> response = bookController.updateBook(id, updateBookDto);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedBook, response.getBody());
    }

    @Test
    public void deleteBookTest() {
        Long id = 1L;

        ResponseEntity<Void> response = bookController.deleteBook(id);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(bookAndBookCopyFacade).deleteBook(id);
    }
}
