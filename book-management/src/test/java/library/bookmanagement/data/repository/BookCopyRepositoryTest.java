package library.bookmanagement.data.repository;

import library.bookmanagement.data.model.BookCopy;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class BookCopyRepositoryTest {

	@Autowired
	private TestEntityManager testEntityManager;

	@Autowired
	private BookCopyRepository bookCopyRepository;

	@Test
	void findBookCopiesBySignature() {
		BookCopy bookCopy = new BookCopy();
		bookCopy.setSignature("E127");
		bookCopy.setEdition("Third");

		testEntityManager.persist(bookCopy);
		testEntityManager.flush();

		List<BookCopy> foundCopies = bookCopyRepository.findBookCopiesBySignature("E127");
		assertThat(foundCopies).hasSize(1).extracting(BookCopy::getSignature).containsOnly(bookCopy.getSignature());
		assertThat(foundCopies).extracting(BookCopy::getEdition).containsOnly(bookCopy.getEdition());
	}

	@Test
	void findBookCopiesByStatus() {
		BookCopy bookCopy = new BookCopy();
		bookCopy.setSignature("E127");
		bookCopy.setStatus(BookCopy.Status.AVAILABLE);

		testEntityManager.persist(bookCopy);
		testEntityManager.flush();

		List<BookCopy> foundCopies = bookCopyRepository.findBookCopiesByStatus(BookCopy.Status.AVAILABLE);
		assertThat(foundCopies).hasSize(1).extracting(BookCopy::getStatus).containsOnly(bookCopy.getStatus());
	}
}