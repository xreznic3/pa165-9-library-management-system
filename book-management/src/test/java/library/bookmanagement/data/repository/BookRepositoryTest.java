package library.bookmanagement.data.repository;

import library.bookmanagement.data.model.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

@DataJpaTest
class BookRepositoryTest {

	@Autowired
	private TestEntityManager testEntityManager;

	@Autowired
	private BookRepository bookRepository;

	@Test
	void findBooksByAuthor() {
		Book book = new Book();
		book.setAuthor("James Clear");
		book.setTitle("Atomic Habits");

		testEntityManager.persist(book);
		testEntityManager.flush();

		List<Book> foundBooks = bookRepository.findBooksByAuthor("James Clear");
		assertThat(foundBooks).hasSize(1).extracting(Book::getAuthor).containsOnly(book.getAuthor());
	}

	@Test
	void findBooksByPublisher() {
		Book book = new Book();
		book.setAuthor("James Clear");
		book.setTitle("Atomic Habits");
		book.setPublisher("Jan Melvil Publishing");

		testEntityManager.persist(book);
		testEntityManager.flush();

		List<Book> foundBooks = bookRepository.findBooksByPublisher("Jan Melvil Publishing");
		assertThat(foundBooks).hasSize(1).extracting(Book::getPublisher).containsOnly(book.getPublisher());
	}

	@Test
	void findBooksWithSetPublisherAndGenre() {
		Book book = new Book();
		book.setTitle("Vitkuv Cestopiss");
		book.setGenre("Cestopis");
		book.setPublisher("Jan Melvil Publishing");
		Book bookWithoutGenreAndPublisher = new Book();
		bookWithoutGenreAndPublisher.setTitle("Empty Book");

		testEntityManager.persist(book);
		testEntityManager.persist(bookWithoutGenreAndPublisher);
		testEntityManager.flush();

		List<Book> foundBooks = bookRepository.findBooksWithSetPublisherAndGenre();
		assertThat(foundBooks).hasSize(1).extracting(Book::getPublisher).containsOnly(book.getPublisher());
	}

	@Test
	void findBooksWithTitleLengthLessThanTenChars() {
		Book book1 = new Book();
		book1.setTitle("Short1");
		Book book2 = new Book();
		book2.setTitle("Short2");
		Book book3 = new Book();
		book3.setTitle("Long title book");

		testEntityManager.persist(book1);
		testEntityManager.persist(book2);
		testEntityManager.persist(book3);
		testEntityManager.flush();

		List<Book> foundBooks = bookRepository.findBooksWithTitleLengthLessThanTenChars();
		assertThat(foundBooks).hasSize(2).extracting(Book::getTitle).containsOnly("Short1", "Short2");

	}
}