package library.bookmanagement.service;

import library.bookmanagement.data.model.Book;
import library.bookmanagement.data.repository.BookRepository;
import library.bookmanagement.dto.CreateBookDto;
import library.bookmanagement.dto.UpdateBookDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class BookManagementServiceIntegrationTest {

	@Autowired
	BookManagementService bookManagementService;

	@Autowired
	BookRepository bookRepository;

	@Test
	public void createBook() {
		CreateBookDto bookDto = new CreateBookDto();
		bookDto.setTitle("Atomic Habits");

		Book savedBook = bookManagementService.createBook(bookDto);
		Book foundBook = bookRepository.findById(savedBook.getId()).orElse(null);

		assertThat(foundBook).isNotNull();
		assert foundBook != null;
		assertThat(foundBook.getTitle()).isEqualTo("Atomic Habits");
	}

	@Test
	public void getBookById() {
		Book book = new Book();
		book.setTitle("Bahamy");

		bookRepository.save(book);
		Book foundBook = bookManagementService.getBookById(book.getId());

		assertThat(foundBook).isNotNull();
		assertThat(foundBook.getTitle()).isEqualTo("Bahamy");
	}

	@Test
	public void updateBook()
	{
		Book originalBook = new Book();
		originalBook.setTitle("Bahamy");

		bookRepository.save(originalBook);

		UpdateBookDto updateBookDto = new UpdateBookDto();
		updateBookDto.setTitle("Galapagy");

		bookManagementService.updateBook(originalBook.getId(), updateBookDto);

		Book foundUpdatedBook = bookRepository.findById(originalBook.getId()).orElse(null);
		assertThat(foundUpdatedBook.getTitle()).isEqualTo("Galapagy");
	}
}
