package library.bookmanagement.service;

import library.bookmanagement.data.model.BookCopy;
import library.bookmanagement.data.repository.BookCopyRepository;
import library.bookmanagement.dto.CreateBookCopyDto;
import library.bookmanagement.dto.UpdateBookCopyDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BookCopyServiceTest {

    @Mock
    private BookCopyRepository bookCopyRepository;

    private BookCopyService bookCopyService;

    @BeforeEach
    void setUp() {
        bookCopyRepository = Mockito.mock(BookCopyRepository.class);
        bookCopyService = new BookCopyServiceImpl(bookCopyRepository);
    }

    @Test
    public void getAllBookCopiesTest() {
        ArrayList<BookCopy> expectedBookCopies = new ArrayList<>();
        when(bookCopyRepository.findAll()).thenReturn(expectedBookCopies);

        Collection<BookCopy> actualBookCopies = bookCopyService.getAllBookCopies();

        assertEquals(expectedBookCopies, actualBookCopies);
    }

    @Test
    public void getBookCopyByIdTest() {
        Long id = 1L;
        BookCopy expectedBookCopy = new BookCopy();
        when(bookCopyRepository.findById(id)).thenReturn(Optional.of(expectedBookCopy));

        BookCopy actualBookCopy = bookCopyService.getBookCopyById(id);

        assertEquals(expectedBookCopy, actualBookCopy);
    }

    @Test
    public void createBookCopyTest() {
        CreateBookCopyDto createBookCopyDto = new CreateBookCopyDto();
        BookCopy expectedBookCopy = new BookCopy();
        when(bookCopyRepository.save(any())).thenReturn(expectedBookCopy);

        BookCopy actualBookCopy = bookCopyService.createBookCopy(createBookCopyDto);

        assertEquals(expectedBookCopy, actualBookCopy);
    }

    @Test
    public void testUpdateBookCopy() {
        UpdateBookCopyDto updateBookCopyDto = new UpdateBookCopyDto();
        BookCopy existingBookCopy = new BookCopy();

        when(bookCopyRepository.findById(any())).thenReturn(Optional.of(existingBookCopy));
        when(bookCopyRepository.save(any(BookCopy.class))).thenReturn(existingBookCopy);

        BookCopy updatedBookCopy = bookCopyService.updateBookCopy(any(), updateBookCopyDto);

        assertEquals(existingBookCopy, updatedBookCopy);
        verify(bookCopyRepository).save(existingBookCopy);
    }
    @Test
    public void deleteBookCopyTest() {
        Long id = 1L;
        BookCopy bookCopyToDelete = new BookCopy();
        when(bookCopyRepository.findById(id)).thenReturn(Optional.of(bookCopyToDelete));

        bookCopyService.deleteBookCopy(id);

        verify(bookCopyRepository).delete(bookCopyToDelete);
    }
}
