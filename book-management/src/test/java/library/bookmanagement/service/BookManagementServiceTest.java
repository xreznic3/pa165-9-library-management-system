package library.bookmanagement.service;

import library.bookmanagement.data.model.Book;
import library.bookmanagement.data.repository.BookRepository;
import library.bookmanagement.dto.CreateBookDto;
import library.bookmanagement.dto.UpdateBookDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

public class BookManagementServiceTest {

    BookRepository bookRepository;

    BookManagementService bookManagementService;
    @BeforeEach
    void setUp() {
        bookRepository = Mockito.mock(BookRepository.class);
        bookManagementService = new BookManagementServiceImpl(bookRepository);
    }

    private Book NewBook(){
        Book book = new Book();
        book.setId(1L);
        book.setTitle("Eragon");
        return book;
    }

    private CreateBookDto CreateBook(){
        CreateBookDto book = new CreateBookDto();
        book.setTitle("Eragon");
        return book;
    }

    @Test
    public void GetBookByIdTest(){
        Book exampleBook = NewBook();
        Mockito.when(bookRepository.findById(1L)).thenReturn(Optional.of(exampleBook));

        Book result = bookManagementService.getBookById(1L);

        assertEquals(result, exampleBook);
    }

    @Test
    public void GetAllBooksTest(){
        ArrayList<Book> books = new ArrayList<>() {
        };
        books.add(NewBook());
        Mockito.when(bookRepository.findAll()).thenReturn(books);

        Collection<Book> result_books = bookManagementService.getAllBooks();

        assertEquals(books, result_books);
    }

    @Test
    public void CreateBookTest(){

        Book book = NewBook();
        CreateBookDto newBook = CreateBook();
        Mockito.when(bookRepository.save(any(Book.class))).thenReturn(book);

        Book createdBook = bookManagementService.createBook(newBook);

        assertEquals(book, createdBook);
        Mockito.verify(bookRepository).save(any(Book.class));
    }

    @Test
    public void UpdateBookTest(){
        UpdateBookDto existingBook = new UpdateBookDto();
        existingBook.setTitle("Updated Title");

        Book updBook = NewBook();
        updBook.setTitle("Updated Title");

        Mockito.when(bookRepository.findById(any())).thenReturn(Optional.of(updBook));
        Mockito.when(bookRepository.save(any())).thenReturn(updBook);

        Book updatedBook = bookManagementService.updateBook(any(), existingBook);

        assertEquals(updatedBook, updBook);
        Mockito.verify(bookRepository).save(updatedBook);
    }

}
