package library.bookmanagement.facade;

import library.bookmanagement.data.model.Book;
import library.bookmanagement.data.model.BookCopy;
import library.bookmanagement.dto.CreateBookCopyDto;
import library.bookmanagement.dto.CreateBookDto;
import library.bookmanagement.dto.UpdateBookCopyDto;

import library.bookmanagement.dto.UpdateBookDto;
import library.bookmanagement.service.BookCopyService;
import library.bookmanagement.service.BookManagementService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BookAndBookCopyFacadeTest {

	@Mock
	private BookManagementService bookManagementService;

	@Mock
	private BookCopyService bookCopyService;

	@InjectMocks
	private BookAndBookCopyFacade facade;

	@Test
	void getAllBooks() {
		Book book1 = new Book();
		book1.setTitle("Book NO1");
		Book book2 = new Book();
		book2.setTitle("Book N02");

		when(bookManagementService.getAllBooks()).thenReturn(Arrays.asList(book1, book2));

		Collection<Book> books = facade.getAllBooks();
		assertNotNull(books);
		assertEquals(2, books.size());
		verify(bookManagementService).getAllBooks();
	}

	@Test
	void getBookById() {
		Long bookId = 1L;
		Book book = new Book();
		book.setId(bookId);
		book.setTitle("Book with ID1");

		when(bookManagementService.getBookById(bookId)).thenReturn(book);

		Book result = facade.getBookById(bookId);

		assertNotNull(result);
		assertEquals("Book with ID1", result.getTitle());
	}

	@Test
	void createBook() {
		CreateBookDto createBookDto = new CreateBookDto();
		createBookDto.setTitle("New Book");

		Book expectedBook = new Book();
		expectedBook.setTitle("New Book");

		when(bookManagementService.createBook(createBookDto)).thenReturn(expectedBook);

		Book result = facade.createBook(createBookDto);

		assertEquals(result.getTitle(), expectedBook.getTitle());
	}

	@Test
	void updateBook() {
		Long bookId = 2L;
		UpdateBookDto updateBookDto = new UpdateBookDto();
		updateBookDto.setTitle("Praha");
		Book updatedBook = new Book();
		updatedBook.setId(bookId);
		updatedBook.setTitle("Praha");

		when(bookManagementService.updateBook(eq(bookId), any(UpdateBookDto.class))).thenReturn(updatedBook);

		Book result = facade.updateBook(bookId, updateBookDto);
		assertEquals(result.getTitle(), "Praha");
	}

	@Test
	void deleteBook() {
		Long bookId = 1L;
		doNothing().when(bookManagementService).deleteBook(bookId);

		facade.deleteBook(bookId);

		verify(bookManagementService).deleteBook(bookId);
	}

	@Test
	void getAllBookCopies() {
		BookCopy bookCopy1 = new BookCopy();
		BookCopy bookCopy2 = new BookCopy();

		when(bookCopyService.getAllBookCopies()).thenReturn(Arrays.asList(bookCopy1, bookCopy2));

		Collection<BookCopy> bookCopies = facade.getAllBookCopies();

		assertNotNull(bookCopies);
		assertEquals(2, bookCopies.size());
		verify(bookCopyService).getAllBookCopies();
	}

	@Test
	void getBookCopyById() {
		Long bookCopyId = 1L;
		BookCopy bookCopy = new BookCopy();
		bookCopy.setId(bookCopyId);
		bookCopy.setSignature("E127");

		when(bookCopyService.getBookCopyById(bookCopyId)).thenReturn(bookCopy);

		BookCopy result = facade.getBookCopyById(bookCopyId);

		assertNotNull(result);
		assertEquals(result.getSignature(), "E127");
	}

	@Test
	void createBookCopy() {
		CreateBookCopyDto createBookCopyDto = new CreateBookCopyDto();
		createBookCopyDto.setSignature("E545");

		BookCopy expectedBookCopy = new BookCopy();
		expectedBookCopy.setSignature("E545");

		when(bookCopyService.createBookCopy(createBookCopyDto)).thenReturn(expectedBookCopy);

		BookCopy result = facade.createBookCopy(createBookCopyDto);
		assertEquals(result.getSignature(), expectedBookCopy.getSignature());
	}

	@Test
	void updateBookCopy() {
		Long bookCopyId = 6L;
		UpdateBookCopyDto updateBookCopyDto = new UpdateBookCopyDto();
		updateBookCopyDto.setSignature("E111");

		BookCopy updatedBookCopy = new BookCopy();
		updatedBookCopy.setId(bookCopyId);
		updatedBookCopy.setSignature("E111");

		when(bookCopyService.updateBookCopy(eq(bookCopyId), any(UpdateBookCopyDto.class))).thenReturn(updatedBookCopy);

		BookCopy result = facade.updateBookCopy(bookCopyId, updateBookCopyDto);

		assertNotNull(result);
		assertEquals(result.getSignature(), "E111");
	}

	@Test
	void deleteBookCopy() {
		Long bookCopyId = 1L;

		doNothing().when(bookCopyService).deleteBookCopy(bookCopyId);

		facade.deleteBookCopy(bookCopyId);

		verify(bookCopyService).deleteBookCopy(bookCopyId);
	}
}