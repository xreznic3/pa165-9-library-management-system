package library.bookmanagement.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import library.bookmanagement.data.model.Book;
import library.bookmanagement.data.model.BookCopy;
import library.bookmanagement.dto.CreateBookCopyDto;
import library.bookmanagement.dto.UpdateBookCopyDto;
import library.bookmanagement.facade.BookAndBookCopyFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("book-copies")
public class BookCopyController {

	private final BookAndBookCopyFacade bookAndBookCopyFacade;

	@Autowired
	public BookCopyController(BookAndBookCopyFacade bookAndBookCopyFacade) {
		this.bookAndBookCopyFacade = bookAndBookCopyFacade;
	}

	@GetMapping
	@Operation(
			operationId = "getAllBookCopies",
			summary = "Get all book copies",
			tags = { "BookCopyController" },
			responses = {
					@ApiResponse(responseCode = "200", description = "OK", content = {
							@Content(mediaType = "application/json", schema = @Schema(implementation = BookCopy.class))
					}),
					@ApiResponse(responseCode = "204", description = "No content")
			}
	)
	public ResponseEntity<Collection<BookCopy>> getAllBookCopies() {
		Collection<BookCopy> bookCopies = bookAndBookCopyFacade.getAllBookCopies();
		if (bookCopies.isEmpty()) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(bookCopies);
	}

	@GetMapping("/{id}")
	@Operation(
			operationId = "getBookCopyById",
			summary = "Gets book copy by id",
			tags = { "BookCopyController" },
			responses = {
					@ApiResponse(responseCode = "200", description = "OK", content = {
							@Content(mediaType = "application/json", schema = @Schema(implementation = BookCopy.class))
					}),
					@ApiResponse(responseCode = "404", description = "Book copy not found")
			}
	)
	public ResponseEntity<BookCopy> getBookCopyById(@PathVariable Long id) {
		BookCopy bookCopy = bookAndBookCopyFacade.getBookCopyById(id);
		if (bookCopy == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(bookCopy);
	}

	@PostMapping
	@Operation(
			operationId = "createBookCopy",
			summary = "Create a new book copy",
			security = { @SecurityRequirement(name = "Bearer Authentication") },
			tags = { "BookCopyController" },
			responses = {
					@ApiResponse(responseCode = "201", description = "Created", content = {
							@Content(mediaType = "application/json", schema = @Schema(implementation = BookCopy.class))
					})
			}
	)
	public ResponseEntity<BookCopy> createBookCopy(@RequestBody CreateBookCopyDto createBookCopyDto) {
		BookCopy bookCopy = bookAndBookCopyFacade.createBookCopy(createBookCopyDto);
		return new ResponseEntity<>(bookCopy, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@Operation(
			operationId = "updateBookCopy",
			summary = "Update an existing book copy",
			security = { @SecurityRequirement(name = "Bearer Authentication") },
			tags = { "BookCopyController" },
			responses = {
					@ApiResponse(responseCode = "200", description = "OK", content = {
							@Content(mediaType = "application/json", schema = @Schema(implementation = BookCopy.class))
					})
			}
	)
	public ResponseEntity<BookCopy> updateBookCopy(@PathVariable Long id, @RequestBody UpdateBookCopyDto updateBookCopyDto) {
		BookCopy bookCopy = bookAndBookCopyFacade.updateBookCopy(id, updateBookCopyDto);
		return new ResponseEntity<>(bookCopy, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	@Operation(
			operationId = "deleteBookCopy",
			summary = "Delete book copy by ID",
			security = { @SecurityRequirement(name = "Bearer Authentication") },
			tags = { "BookCopyController" },
			responses = {
					@ApiResponse(responseCode = "204", description = "No content")
			}
	)
	public ResponseEntity<Void> deleteBookCopy(@PathVariable Long id) {
		bookAndBookCopyFacade.deleteBookCopy(id);
		return ResponseEntity.noContent().build();
	}
}
