package library.bookmanagement.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import library.bookmanagement.data.model.Book;
import library.bookmanagement.dto.CreateBookDto;
import library.bookmanagement.dto.UpdateBookDto;
import library.bookmanagement.facade.BookAndBookCopyFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/books")
public class BookController {

	private final BookAndBookCopyFacade bookAndBookCopyFacade;

	@Autowired
	public BookController(BookAndBookCopyFacade bookAndBookCopyFacade)
	{
		this.bookAndBookCopyFacade = bookAndBookCopyFacade;
	}

	@GetMapping
	@Operation(
			operationId = "getAllBooks",
			summary = "Get all books",
			tags = { "BookController" },
			responses = {
					@ApiResponse(responseCode = "200", description = "OK", content = {
							@Content(mediaType = "application/json", schema = @Schema(implementation = Book.class))
					}),
					@ApiResponse(responseCode = "204", description = "No content")
			}
	)
	public ResponseEntity<Collection<Book>> getAllBooks() {
		Collection<Book> books = bookAndBookCopyFacade.getAllBooks();
		if (books.isEmpty()) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(books);
	}

	@GetMapping("/{id}")
	@Operation(
			operationId = "getBookById",
			summary = "Gets book by id",
			tags = { "BookController" },
			responses = {
				@ApiResponse(responseCode = "200", description = "OK", content = {
						@Content(mediaType = "application/json", schema = @Schema(implementation = Book.class))
				}),
				@ApiResponse(responseCode = "404", description = "Book not found")
			}
	)
	public ResponseEntity<Book> getBookById(@PathVariable Long id) {
		Book book = bookAndBookCopyFacade.getBookById(id);
		if (book == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok(book);
	}

	@PostMapping
	@Operation(
			operationId = "createBook",
			summary = "Create a new book",
			security = { @SecurityRequirement(name = "Bearer Authentication") },
			tags = { "BookController" },
			responses = {
					@ApiResponse(responseCode = "201", description = "Created", content = {
							@Content(mediaType = "application/json", schema = @Schema(implementation = Book.class))
					})
			}
	)
	public ResponseEntity<Book> createBook(@RequestBody CreateBookDto createBookDto) {
		Book book = bookAndBookCopyFacade.createBook(createBookDto);
		return new ResponseEntity<>(book, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@Operation(
			operationId = "updateBook",
			summary = "Update an existing book",
			security = { @SecurityRequirement(name = "Bearer Authentication") },
			tags = { "BookController" },
			responses = {
					@ApiResponse(responseCode = "200", description = "OK", content = {
							@Content(mediaType = "application/json", schema = @Schema(implementation = Book.class))
					})
			}
	)
	public ResponseEntity<Book> updateBook(@PathVariable Long id, @RequestBody UpdateBookDto updateBookDto) {
		Book book = bookAndBookCopyFacade.updateBook(id, updateBookDto);
		return new ResponseEntity<>(book, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	@Operation(
			operationId = "deleteBook",
			summary = "Delete book by ID",
			security = { @SecurityRequirement(name = "Bearer Authentication") },
			tags = { "BookController" },
			responses = {
					@ApiResponse(responseCode = "204", description = "No content")
			}
	)
	public ResponseEntity<Void> deleteBook(@PathVariable Long id) {
		bookAndBookCopyFacade.deleteBook(id);
		return ResponseEntity.noContent().build();
	}
}
