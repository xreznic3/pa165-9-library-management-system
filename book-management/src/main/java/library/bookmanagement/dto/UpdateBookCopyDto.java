package library.bookmanagement.dto;

import library.bookmanagement.data.model.BookCopy;

public class UpdateBookCopyDto {
	private String signature;
	private BookCopy.Status status;
	private String edition;

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public BookCopy.Status getStatus() {
		return status;
	}

	public void setStatus(BookCopy.Status status) {
		this.status = status;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

}
