package library.bookmanagement.dto;

import library.bookmanagement.data.model.Book;
import library.bookmanagement.data.model.BookCopy;

public class CreateBookCopyDto {
	private Book book;
	private String signature;
	private BookCopy.Status status;
	private String edition;

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public BookCopy.Status getStatus() {
		return status;
	}

	public void setStatus(BookCopy.Status status) {
		this.status = status;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

}
