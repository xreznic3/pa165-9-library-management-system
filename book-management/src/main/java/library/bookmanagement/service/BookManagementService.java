package library.bookmanagement.service;

import library.bookmanagement.data.model.Book;
import library.bookmanagement.dto.CreateBookDto;
import library.bookmanagement.dto.UpdateBookDto;

import java.util.Collection;

public interface BookManagementService {

	/**
	 * Get all books stored in database.
	 * @return collection of the books.
	 */
	Collection<Book> getAllBooks();

	/**
	 * @param id of the book
	 * @return book
	 */
	Book getBookById(Long id);

	/**
	 * @param createBookDto book parameters for store in database
	 * @return created book entity
	 */
	Book createBook(CreateBookDto createBookDto);

	/**
	 * @param updateBookDto book parameters for update in database
	 * @param id of book to update
	 * @return updated book entity
	 */
	Book updateBook(Long id, UpdateBookDto updateBookDto);

	/**
	 * @param id of book to delete
	 */
	void deleteBook(Long id);
}
