package library.bookmanagement.service;

import library.bookmanagement.data.mapper.BookMapper;
import library.bookmanagement.data.model.Book;
import library.bookmanagement.data.repository.BookRepository;
import library.bookmanagement.dto.CreateBookDto;
import library.bookmanagement.dto.UpdateBookDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class BookManagementServiceImpl implements BookManagementService {

	private BookRepository bookRepository;

	@Autowired
	public BookManagementServiceImpl(BookRepository bookRepository) {
		super();
		this.bookRepository = bookRepository;
	}

	@Override
	public Collection<Book> getAllBooks() {
		return bookRepository.findAll();
	}

	@Override
	public Book getBookById(Long id) {
		return bookRepository.findById(id).orElse(null);
	}

	@Override
	public Book createBook(CreateBookDto createBookDto) {
		Book book = BookMapper.MapToBookEntity(createBookDto);
		return bookRepository.save(book);
	}

	@Override
	public Book updateBook(Long id, UpdateBookDto updateBookDto) {
		Book book = getBookById(id);
		if (book != null) {
			book.setTitle(updateBookDto.getTitle());
			book.setIsbn(updateBookDto.getIsbn());
			book.setAuthor(updateBookDto.getAuthor());
			book.setPublisher(updateBookDto.getPublisher());
			book.setGenre(updateBookDto.getGenre());
			book.setDatePublished(updateBookDto.getDatePublished());
		}
		return bookRepository.save(book);
	}
	@Override
	public void deleteBook(Long id) {
		Optional<Book> book = bookRepository.findById(id);
		if (book.isPresent()) {
			bookRepository.delete(book.get());
		}
	}


}
