package library.bookmanagement.service;

import library.bookmanagement.data.mapper.BookCopyMapper;
import library.bookmanagement.data.model.BookCopy;
import library.bookmanagement.data.repository.BookCopyRepository;
import library.bookmanagement.dto.CreateBookCopyDto;
import library.bookmanagement.dto.UpdateBookCopyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class BookCopyServiceImpl implements BookCopyService {

	private BookCopyRepository bookCopyRepository;

	@Autowired
	public BookCopyServiceImpl(BookCopyRepository bookCopyRepository) {
		this.bookCopyRepository = bookCopyRepository;
	}

	@Override
	public Collection<BookCopy> getAllBookCopies() {
		return bookCopyRepository.findAll();
	}

	@Override
	public BookCopy getBookCopyById(Long id) {
		return bookCopyRepository.findById(id).orElse(null);
	}

	@Override
	public BookCopy createBookCopy(CreateBookCopyDto createBookCopyDto) {
		BookCopy bookCopy = BookCopyMapper.MapToBookCopyEntity(createBookCopyDto);
		return bookCopyRepository.save(bookCopy);
	}

	@Override
	public BookCopy updateBookCopy(Long id, UpdateBookCopyDto updateBookCopyDto) {
		BookCopy bookCopy = getBookCopyById(id);
		if (bookCopy != null) {
			bookCopy.setSignature(updateBookCopyDto.getSignature());
			bookCopy.setStatus(updateBookCopyDto.getStatus());
			bookCopy.setEdition(updateBookCopyDto.getEdition());
		}
		return bookCopyRepository.save(bookCopy);
	}

	@Override
	public void deleteBookCopy(Long id) {
		Optional<BookCopy> bookCopy = bookCopyRepository.findById(id);
		if (bookCopy.isPresent()) {
			bookCopyRepository.delete(bookCopy.get());
		}
	}
}
