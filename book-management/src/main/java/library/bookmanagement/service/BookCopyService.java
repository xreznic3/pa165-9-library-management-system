package library.bookmanagement.service;

import library.bookmanagement.data.model.BookCopy;
import library.bookmanagement.dto.CreateBookCopyDto;
import library.bookmanagement.dto.UpdateBookCopyDto;

import java.util.Collection;

public interface BookCopyService {

	/**
	 * Get all book copies stored in database.
	 * @return Collection of the book copies.
	 */
	Collection<BookCopy> getAllBookCopies();

	/**
	 * @param id ID of book copy
	 * @return Book copy
	 */
	BookCopy getBookCopyById(Long id);

	/**
	 * @param createBookCopyDto book copy parameter for store in database
	 * @return stored book copy
	 */
	BookCopy createBookCopy(CreateBookCopyDto createBookCopyDto);

	/**
	 * @param id of book copy to update
	 * @param updateBookCopyDto book copy parameter for update in database
	 * @return updated book copy
	 */
	BookCopy updateBookCopy(Long id, UpdateBookCopyDto updateBookCopyDto);

	/**
	 * @param id of book copy to delete
	 */
	void deleteBookCopy(Long id);
}
