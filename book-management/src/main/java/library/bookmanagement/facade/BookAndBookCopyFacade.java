package library.bookmanagement.facade;

import library.bookmanagement.data.model.Book;
import library.bookmanagement.data.model.BookCopy;
import library.bookmanagement.dto.CreateBookCopyDto;
import library.bookmanagement.dto.CreateBookDto;
import library.bookmanagement.dto.UpdateBookCopyDto;
import library.bookmanagement.dto.UpdateBookDto;
import library.bookmanagement.service.BookCopyService;
import library.bookmanagement.service.BookManagementService;
import org.hibernate.sql.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class BookAndBookCopyFacade {
	private BookManagementService bookManagementService;
	private BookCopyService bookCopyService;

	@Autowired
	public BookAndBookCopyFacade(BookManagementService bookManagementService, BookCopyService bookCopyService) {
		this.bookManagementService = bookManagementService;
		this.bookCopyService = bookCopyService;
	}

	public Collection<Book> getAllBooks() {
		return bookManagementService.getAllBooks();
	}
	public Book getBookById(Long id) {
		return bookManagementService.getBookById(id);
	}

	public Book createBook(CreateBookDto createBookDto) {
		return bookManagementService.createBook(createBookDto);
	}

	public Book updateBook(Long id, UpdateBookDto updateBookDto) {
		return bookManagementService.updateBook(id, updateBookDto);
	}

	public void deleteBook(Long id) {
		bookManagementService.deleteBook(id);
	}

	public Collection<BookCopy> getAllBookCopies() {
		return bookCopyService.getAllBookCopies();
	}
	public BookCopy getBookCopyById(Long id) {
		return bookCopyService.getBookCopyById(id);
	}

	public BookCopy createBookCopy(CreateBookCopyDto createBookCopyDto) {
		return bookCopyService.createBookCopy(createBookCopyDto);
	}

	public BookCopy updateBookCopy(Long id, UpdateBookCopyDto updateBookCopyDto) {
		return bookCopyService.updateBookCopy(id, updateBookCopyDto);
	}

	public void deleteBookCopy(Long id) {
		bookCopyService.deleteBookCopy(id);
	}
}
