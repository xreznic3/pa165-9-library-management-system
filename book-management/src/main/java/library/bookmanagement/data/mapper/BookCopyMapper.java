package library.bookmanagement.data.mapper;

import library.bookmanagement.data.model.BookCopy;
import library.bookmanagement.dto.CreateBookCopyDto;

public class BookCopyMapper {
	public static BookCopy MapToBookCopyEntity(CreateBookCopyDto createBookCopyDto) {
		BookCopy bookCopy = new BookCopy();
		bookCopy.setBook(createBookCopyDto.getBook());
		bookCopy.setSignature(createBookCopyDto.getSignature());
		bookCopy.setStatus(createBookCopyDto.getStatus());
		bookCopy.setEdition(createBookCopyDto.getEdition());
		return bookCopy;
	}
}
