package library.bookmanagement.data.mapper;

import library.bookmanagement.data.model.Book;
import library.bookmanagement.dto.CreateBookDto;

public class BookMapper {
	public static Book MapToBookEntity(CreateBookDto createBookDto) {
		Book book = new Book();
		book.setTitle(createBookDto.getTitle());
		book.setIsbn(createBookDto.getIsbn());
		book.setAuthor(createBookDto.getAuthor());
		book.setPublisher(createBookDto.getPublisher());
		book.setGenre(createBookDto.getGenre());
		book.setDatePublished(createBookDto.getDatePublished());
		return book;
	}
}
