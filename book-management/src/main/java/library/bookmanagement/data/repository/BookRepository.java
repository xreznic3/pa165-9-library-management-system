package library.bookmanagement.data.repository;

import library.bookmanagement.data.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
	List<Book> findBooksByAuthor(String author);

	List<Book> findBooksByPublisher(String publisher);

	@Query("SELECT b FROM Book b WHERE b.publisher IS NOT NULL AND b.genre IS NOT NULL")
	List<Book> findBooksWithSetPublisherAndGenre();

	@Query("SELECT b FROM Book b WHERE LENGTH(b.title) < 10")
	List<Book> findBooksWithTitleLengthLessThanTenChars();
}
