package library.bookmanagement.data.repository;

import library.bookmanagement.data.model.BookCopy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookCopyRepository extends JpaRepository<BookCopy, Long> {
	List<BookCopy> findBookCopiesBySignature(String signature);

	List<BookCopy> findBookCopiesByStatus(BookCopy.Status status);
}
