package library.bookmanagement.data.seed;

import library.bookmanagement.data.model.Book;
import library.bookmanagement.data.model.BookCopy;
import library.bookmanagement.data.repository.BookCopyRepository;
import library.bookmanagement.data.repository.BookRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.*;



import static library.bookmanagement.data.model.BookCopy.Status.AVAILABLE;
import static library.bookmanagement.data.model.BookCopy.Status.BORROWED;

@Component

public class DataInitializer implements ApplicationRunner {
    @Autowired
    private BookCopyRepository bookCopyRepository;
    @Autowired
    private BookRepository bookRepository;
    DataGenerator dataGenerator = new DataGenerator();
    static Random random = new Random();

    public static String GenerateIsbn() {
        String[] possibleChars = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "X"};
        StringBuilder isbnBuilder = new StringBuilder();
        for (int i = 0; i < 13; i++) {
            if (i == 3 || i == 5 || i == 11) {
                isbnBuilder.append("-");
            } else {
                isbnBuilder.append(possibleChars[random.nextInt(possibleChars.length)]);
            }
        }
        return isbnBuilder.toString();
    }
    public static String generateRandomDate(int startYear, int endYear) {
        int year = random.nextInt(endYear-startYear)+startYear;
        int month = random.nextInt(12)+1;
        int day = random.nextInt(28)+1;
        return String.valueOf(year)+String.valueOf(month)+String.valueOf(day);
    }


    public void seedBookCopy(int generatedBookCopy) {
        BookCopy.Status[] statuses = {AVAILABLE,BORROWED};
        List<Book> books = bookRepository.findAll();
        for (int i = 0; i < generatedBookCopy; i++){
            BookCopy bookCopy = new BookCopy();
            bookCopy.setBook(books.get(random.nextInt(books.size())));
            bookCopy.setEdition(dataGenerator.getRandomEdition());
            bookCopy.setSignature(dataGenerator.getRandomSignature());
            bookCopy.setStatus(statuses[random.nextInt(statuses.length)]);
            bookCopyRepository.save(bookCopy);
        }
    }

    public void seedBooks(int generatedBooks) {
        for (int i = 0; i < generatedBooks; i++) {
            Book book = new Book();
            book.setTitle(dataGenerator.getRandomTitle());
            book.setIsbn(GenerateIsbn());
            book.setAuthor(dataGenerator.getRandomAuthor());
            book.setPublisher(dataGenerator.getRandomPublisher());
            book.setGenre(dataGenerator.getRandomGender());
            book.setDatePublished(generateRandomDate(1950,2024));
            bookRepository.save(book);
        }
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        seedBooks(50);
        seedBookCopy(50);
    }
}
