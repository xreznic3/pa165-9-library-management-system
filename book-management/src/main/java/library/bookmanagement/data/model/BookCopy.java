package library.bookmanagement.data.model;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name="book_copies")
public class BookCopy {

	public enum Status {
		AVAILABLE,
		BORROWED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", insertable=false, updatable=false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "book_id")
	private Book book;

	@Column(name = "signature")
	private String signature;

	@Column(name = "status")
	private Status status;

	@Column(name = "edition")
	private String edition;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof BookCopy)) {
			return false;
		}
		BookCopy bookCopy = (BookCopy) o;
		return (Objects.equals(bookCopy.id, this.id) && Objects.equals(bookCopy.edition, this.edition));
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, signature, status, edition);
	}
}
