# PA165 9 Library Management System
The Library Management System aims to provide a platform for managing library resources and services. The system allows users to access the library's resources online, search for books, reserve books, borrow and return books, and manage their accounts. The system has two types of users: the librarian and the members. The librarian is responsible for managing the library resources, adding new books, updating book information and managing user accounts. The members can search for books, reserve books, borrow and return books, and manage their accounts. Each borrow has a price and a limit in days for returning, and if this limit is not kept, there is a configurable fine for each delayed day. Anonymous users can only list and search books, but cannot reserve or borrow books without having an account.

## Authors
| Name  | Login |
| ------------- |:----------:|
| Jan Řezníček    | xreznic3 |
|Vojtěch Peschel  |xpesche1  |
|Ondřej Šebesta   |xsebesta  |

## How to work
```mvn clean install && mvn spring-boot:run```

## Use Case diagram
![Use Case diagram](./usecasediagram.svg "Use case diagram.")

## Class diagram
![Class diagram](./classdiagram.svg "Class diagram")

## Modules
### book-management
This microservice handles the management of books and book copies, providing functionality for creating, retrieving, updating, and deleting records through a RESTful API.

### borrow-management
This microservice manages the borrowing of books, offering functionalities to create, update, delete, and retrieve borrow records via a RESTful API. It also calculates fines for overdue borrows, determines return dates based on borrowing terms.

### user-management
This microservice manages user data within a library system, providing RESTful endpoints for adding, editing, retrieving, and deleting user records based on various parameters such as username and user role. It supports complex operations like filtering users by multiple criteria.

### Authentication
For authentication, oidc.muni.cz is used. Firstly, you need to generate bearer token that will be attached to security header of the reqest. Bearer token is generated in user-management module on http://localhost:8080/login. After successful login, you will be redirrected to page where your bearer token will be displayed.

## Docker
Generate module images for all modules starting from the project's root directory, ensuring that the 'mvn clean install' process has completed without any errors:

    docker compose build

Launch containers for all modules from the project's root directory:

    docker compose up

Alternatively, execute it in the background:

    docker compose up -d

Halt all module containers from the project's root directory:

    docker compose down

<<<<<<< README.md
## Locust 
### Run
To run locust simly install it with pip install locust and run locust -f user_scenario.py and open http://localhost:8089/ you can   enter any host name script will rewrite it. 

### User scenario
User scenario is based on first visit of the user. He will simply create his own account after that he will look at the books at the library. After he will find book what he want to borrow he will borrow it. After that he will return the book and check if his book is returned. 


## Grafana and Prometheus
- Prometheus runs on port 9090
- Grafana runs on port 3000

- Grafana has preconfigured data source and one dashboard, no login needed for Grafana