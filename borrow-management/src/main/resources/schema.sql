CREATE TABLE IF NOT EXISTS books (
                                     id INT AUTO_INCREMENT PRIMARY KEY,
                       title VARCHAR(255) NOT NULL,
                       isbn VARCHAR(20),
                       author VARCHAR(255),
                       publisher VARCHAR(255),
                       genre VARCHAR(100),
                       date_published DATE
);
CREATE TABLE IF NOT EXISTS book_copies (
                                           id INT AUTO_INCREMENT PRIMARY KEY,
                             book_id BIGINT REFERENCES books(id),
                             signature VARCHAR(255),
                             status VARCHAR(50),
                             edition VARCHAR(100)
);
CREATE TABLE IF NOT EXISTS users (
                                     id INT AUTO_INCREMENT PRIMARY KEY,
                                     user_name VARCHAR(50) UNIQUE NOT NULL,
                                     role VARCHAR(20) NOT NULL,
                                     password VARCHAR(255) NOT NULL,
                                     first_name VARCHAR(255) NOT NULL,
                                     middle_name VARCHAR(255),
                                     last_name VARCHAR(255) NOT NULL,
                                     email VARCHAR(255)
);
CREATE TABLE IF NOT EXISTS borrows (
                        id INT AUTO_INCREMENT PRIMARY KEY,
                         user_id BIGINT REFERENCES users(id),
                         bookCopy_id BIGINT REFERENCES book_copies(id),
                         borrow_date DATE,
                         return_date DATE,
                         price NUMERIC(10, 2),
                         return_limit_days INT,
                         fine_per_day NUMERIC(10, 2),
                         returned BOOLEAN
);


