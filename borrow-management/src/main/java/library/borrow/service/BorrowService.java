package library.borrow.service;

import library.borrow.api.borrow.CreateBorrowRequestDTO;
import library.borrow.api.borrow.UpdateBorrowRequestDTO;
import library.borrow.data.model.Book;
import library.borrow.data.model.BookCopy;
import library.borrow.data.model.Borrow;
import library.borrow.data.model.User;
import library.borrow.data.repository.BookCopyRepository;
import library.borrow.data.repository.BorrowRepository;
import library.borrow.data.repository.BookRepository;
import library.borrow.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BorrowService {

    @Autowired
    private BorrowRepository borrowRepository;
    @Autowired
    private BookCopyRepository bookCopyRepository;
    @Autowired
    private UserRepository userRepository;

    public Borrow createBorrow(CreateBorrowRequestDTO createBorrowRequestDTO) {
        User user = userRepository.findById(createBorrowRequestDTO.getUserId()).orElse(null);
        BookCopy bookCopy = bookCopyRepository.findById(createBorrowRequestDTO.getCopyBookId()).orElse(null);
        if (user == null) {
            throw new IllegalArgumentException("User doesnt exist");
        }


        Borrow borrow = new Borrow();
        borrow.setUser(user);
        borrow.setBookCopy(bookCopy);
        borrow.setBorrowDate(new Date());
        borrow.setReturnDate(calculateReturnDate(createBorrowRequestDTO.getReturnLimitDays()));
        borrow.setPrice(createBorrowRequestDTO.getPrice());
        borrow.setReturnLimitDays(createBorrowRequestDTO.getReturnLimitDays());
        borrow.setFinePerDay(createBorrowRequestDTO.getFinePerDay());
        borrow.setReturned(false);


        return borrowRepository.save(borrow);
    }
    public Borrow updateBorrow(Long borrowId,UpdateBorrowRequestDTO updateBorrowRequestDTO) {
        BigDecimal price = updateBorrowRequestDTO.getPrice();
        int returnLimitDays = updateBorrowRequestDTO.getReturnLimitDays();
        Date borrowDate = updateBorrowRequestDTO.getBorrowDate();
        BigDecimal finePerDay = updateBorrowRequestDTO.getFinePerDay();

        Borrow existingBorrow = getBorrowById(borrowId);
        if (existingBorrow == null) {
            throw new IllegalArgumentException("Borrow doesnt exist");
        }
        existingBorrow.setBorrowDate(borrowDate);
        existingBorrow.setReturnDate(calculateReturnDate(returnLimitDays));
        existingBorrow.setPrice(price);
        existingBorrow.setReturnLimitDays(returnLimitDays);
        existingBorrow.setFinePerDay(finePerDay);
        return borrowRepository.save(existingBorrow);
    }


    public Borrow getBorrowById(Long borrowId) {
        return borrowRepository.findById(borrowId).orElse(null);
    }

    private Date calculateReturnDate(int returnLimitDays) {

        long returnDateMillis = System.currentTimeMillis() + ((long) returnLimitDays * 24 * 60 * 60 * 1000);
        return new Date(returnDateMillis);
    }
    public List<Borrow> getOverdueBorrows() {
        Date currentDate = new Date();

        List<Borrow> overdueBorrows = borrowRepository.getOverdueBorrows(currentDate);

        return overdueBorrows;
    }
    public BigDecimal calculateFine(Borrow borrow) {
        if (borrow.isReturned()) {
            return BigDecimal.ZERO;
        }

        Date currentDate = new Date();
        if (currentDate.after(borrow.getReturnDate())) {
            long daysOverdue = (currentDate.getTime() - borrow.getReturnDate().getTime()) / (24 * 60 * 60 * 1000);
            return borrow.getFinePerDay().multiply(BigDecimal.valueOf(daysOverdue));
        } else {
            return BigDecimal.ZERO;
        }
    }

    public boolean returnBook(Long borrowId) {
        Borrow borrow = getBorrowById(borrowId);
        if (borrow == null) {
            throw new IllegalArgumentException("Borrow doesn't exist");
        }

        borrow.setReturned(true);
        borrowRepository.save(borrow);
        return true;
    }

    public boolean deleteBorrow(Long borrowId) {
        Optional<Borrow> optionalBorrow = borrowRepository.findById(borrowId);
        if (optionalBorrow.isPresent()) {
            borrowRepository.delete(optionalBorrow.get());
            return true;
        } else {
            return false;
        }
    }

}