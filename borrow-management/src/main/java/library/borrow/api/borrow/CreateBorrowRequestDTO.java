package library.borrow.api.borrow;


import java.math.BigDecimal;

public class CreateBorrowRequestDTO {


    private Long userId;


    private Long bookCopyId;


    private BigDecimal price;


    private Integer returnLimitDays;


    private BigDecimal finePerDay;

 

    public CreateBorrowRequestDTO() {
    }

    public CreateBorrowRequestDTO(Long userId, Long bookCopyId, BigDecimal price, Integer returnLimitDays, BigDecimal finePerDay) {
        this.userId = userId;
        this.bookCopyId = bookCopyId;
        this.price = price;
        this.returnLimitDays = returnLimitDays;
        this.finePerDay = finePerDay;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCopyBookId() {
        return bookCopyId;
    }

    public void setBookId(Long bookCopyId) {
        this.bookCopyId = bookCopyId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getReturnLimitDays() {
        return returnLimitDays;
    }

    public void setReturnLimitDays(Integer returnLimitDays) {
        this.returnLimitDays = returnLimitDays;
    }

    public BigDecimal getFinePerDay() {
        return finePerDay;
    }

    public void setFinePerDay(BigDecimal finePerDay) {
        this.finePerDay = finePerDay;
    }
}
