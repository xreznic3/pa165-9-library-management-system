package library.borrow.api.borrow;

import java.math.BigDecimal;
import java.util.Date;

public class UpdateBorrowRequestDTO {
    private Long borrowId;
    private BigDecimal price;
    private Date borrowDate;
    private int returnLimitDays;
    private BigDecimal finePerDay;


    public Long getBorrowId() {
        return borrowId;
    }

    public void setBorrowId(Long borrowId) {
        this.borrowId = borrowId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getReturnLimitDays() {
        return returnLimitDays;
    }

    public void setReturnLimitDays(int returnLimitDays) {
        this.returnLimitDays = returnLimitDays;
    }

    public BigDecimal getFinePerDay() {
        return finePerDay;
    }

    public void setFinePerDay(BigDecimal finePerDay) {
        this.finePerDay = finePerDay;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }
}