package library.borrow.api.user;

public enum UserRole {
    MEMBER,
    LIBRARIAN
}
