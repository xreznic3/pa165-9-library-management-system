package library.borrow.facade;

import library.borrow.api.borrow.CreateBorrowRequestDTO;
import library.borrow.api.borrow.UpdateBorrowRequestDTO;
import library.borrow.data.model.Borrow;
import library.borrow.data.model.User;
import library.borrow.data.repository.BorrowRepository;
import library.borrow.mappers.BorrowMapper;
import library.borrow.service.BorrowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import library.borrow.data.model.Book;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class BorrowFacade {

    @Autowired
    private BorrowRepository borrowRepository;
    @Autowired
    private BorrowService borrowService;

    public Borrow createBorrow(CreateBorrowRequestDTO createBorrowRequestDTO) {
        return borrowService.createBorrow(createBorrowRequestDTO);
    }

    public Borrow updateBorrow(Long borrowId,UpdateBorrowRequestDTO updateBorrowRequestDTO) {
        return borrowService.updateBorrow(borrowId,updateBorrowRequestDTO);
    }

    public Borrow getBorrowById(Long borrowId) {
        return borrowService.getBorrowById(borrowId);
    }

    public boolean returnBook(Long borrowId) {return borrowService.returnBook(borrowId); }

    private Date calculateReturnDate(int returnLimitDays) {
        long returnDateMillis = System.currentTimeMillis() + (returnLimitDays * 24 * 60 * 60 * 1000);
        return new Date(returnDateMillis);
    }

    public boolean deleteBorrow(Long borrowId) {
        return borrowService.deleteBorrow(borrowId);
    }

    public List<Borrow> getAllBorrows() {
        return borrowRepository.findAll();
    }
    public List<Borrow> getOverdueBorrows()  {
        return borrowService.getOverdueBorrows();
    }
}