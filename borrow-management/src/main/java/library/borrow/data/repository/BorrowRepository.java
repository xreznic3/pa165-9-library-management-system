package library.borrow.data.repository;

import library.borrow.data.model.Borrow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface BorrowRepository extends JpaRepository<Borrow, Long> {


    @Query("SELECT b FROM Borrow b WHERE b.returnLimitDays < 0 AND b.returned = false")
    List<Borrow> getOverdueBorrows(@Param("currentDate") Date currentDate);
    @Query("SELECT b FROM Borrow b WHERE b.user.id = :userId AND b.returnLimitDays < 0 AND b.returned = false")
    List<Borrow> findOverdueBorrowsWithoutFines(@Param("userId") Long userId);
}