package library.borrow.data.repository;


import library.borrow.data.model.BookCopy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.util.Optional;

@Repository
public interface BookCopyRepository extends JpaRepository<BookCopy, Long> {
    @Query("SELECT b FROM Book b WHERE b.id = :id")
    Optional<BookCopy> findById(@Param("id") Long id);

}