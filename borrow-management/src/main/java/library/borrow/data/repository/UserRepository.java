package library.borrow.data.repository;

import library.borrow.data.model.Book;
import library.borrow.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query("SELECT b FROM User b WHERE b.id = :id")
    Optional<User> findById(@Param("id") Long id);
}