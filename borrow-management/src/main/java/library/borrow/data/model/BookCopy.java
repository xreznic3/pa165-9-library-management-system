package library.borrow.data.model;

import jakarta.persistence.*;

@Entity
@Table(name="book_copies")
public class BookCopy {

    public enum Status {
        AVAILABLE,
        BORROWED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", insertable=false, updatable=false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }


}
