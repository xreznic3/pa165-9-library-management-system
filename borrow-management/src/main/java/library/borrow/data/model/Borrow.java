package library.borrow.data.model;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Entity
public class Borrow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "bookCopy_id")
    private BookCopy bookCopy;

    private Date borrowDate;


    private Date returnDate;

    private BigDecimal price;

    private int returnLimitDays;

    private BigDecimal finePerDay;

    private boolean returned;
    public User getUser() {
        return user;
    }


    public Date getBorrowDate() {
        return borrowDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public int getReturnLimitDays() {
        return returnLimitDays;
    }

    public BigDecimal getFinePerDay() {
        return finePerDay;
    }

    public boolean isReturned() {
        return returned;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setReturnLimitDays(int returnLimitDays) {
        this.returnLimitDays = returnLimitDays;
    }

    public void setFinePerDay(BigDecimal finePerDay) {
        this.finePerDay = finePerDay;
    }

    public void setReturned(boolean returned) {
        this.returned = returned;
    }

    public BookCopy getBookCopy() {
        return bookCopy;
    }

    public void setBookCopy(BookCopy bookCopy) {
        this.bookCopy = bookCopy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Borrow)) {
            return false;
        }
        Borrow borrow = (Borrow) o;
        return (Objects.equals(borrow.id, this.id) && Objects.equals(borrow.borrowDate, this.borrowDate));
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, borrowDate);
    }
}