package library.borrow.data.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import library.borrow.api.user.UserRole;


@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}

