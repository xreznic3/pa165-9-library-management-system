package library.borrow.data.seed;

import library.borrow.api.user.UserRole;
import library.borrow.data.model.Book;
import library.borrow.data.model.BookCopy;
import library.borrow.data.model.Borrow;
import library.borrow.data.model.User;
import library.borrow.data.repository.BookCopyRepository;
import library.borrow.data.repository.BookRepository;
import library.borrow.data.repository.BorrowRepository;
import library.borrow.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.origin.SystemEnvironmentOrigin;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import static library.borrow.api.user.UserRole.LIBRARIAN;
import static library.borrow.api.user.UserRole.MEMBER;
import static library.borrow.data.model.BookCopy.Status.AVAILABLE;
import static library.borrow.data.model.BookCopy.Status.BORROWED;


@Component
public class DataInitializer implements ApplicationRunner {
    @Autowired
    private BorrowRepository borrowRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BookCopyRepository bookCopyRepository;
    @Autowired
    private BookRepository bookRepository;
    DataGenerator dataGenerator = new DataGenerator();
    static Random random = new Random();

    public static String GenerateIsbn() {
        String[] possibleChars = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "X"};
        StringBuilder isbnBuilder = new StringBuilder();
        for (int i = 0; i < 13; i++) {
            if (i == 3 || i == 5 || i == 11) {
                isbnBuilder.append("-");
            } else {
                isbnBuilder.append(possibleChars[random.nextInt(possibleChars.length)]);
            }
        }
        return isbnBuilder.toString();
    }
    public static String generateRandomDate(int startYear, int endYear) {
        int year = random.nextInt(endYear-startYear)+startYear;
        int month = random.nextInt(12)+1;
        int day = random.nextInt(28)+1;
        return String.valueOf(year)+String.valueOf(month)+String.valueOf(day);
    }

    public static Date between(Date startInclusive, Date endExclusive) {
        long startMillis = startInclusive.getTime();
        long endMillis = endExclusive.getTime();
        long randomMillisSinceEpoch = ThreadLocalRandom
                .current()
                .nextLong(startMillis, endMillis);

        return new Date(randomMillisSinceEpoch);
    }
    public static String generatePassword(int length) {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+";
        Random random = new Random();
        StringBuilder password = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(characters.length());
            password.append(characters.charAt(index));
        }

        return password.toString();
    }
    public void seedBorrows(int generatedBorrows) {
        List<User> users = userRepository.findAll();
        List<BookCopy> bookCopies = bookCopyRepository.findAll();
        boolean[] randomBoolean= {true,false};
        for(int i = 0; i < generatedBorrows; i++){
            Borrow borrow = new Borrow();
            borrow.setReturned(randomBoolean[random.nextInt(2)]);
            Date startDate = new GregorianCalendar(2015, Calendar.APRIL, 1).getTime();
            Date endDate = new GregorianCalendar(2024, Calendar.APRIL, 1).getTime();
            Date borrowDate = between(startDate,endDate);
            borrow.setBorrowDate(borrowDate);
            endDate = new GregorianCalendar(2024, Calendar.APRIL, 2).getTime();
            borrow.setReturnDate(between(borrowDate,endDate));
            borrow.setFinePerDay(BigDecimal.valueOf(random.nextDouble(10.0)));
            borrow.setPrice(BigDecimal.valueOf(random.nextDouble(10.0)));
            borrow.setUser(users.get(random.nextInt(users.size())));
            borrow.setBookCopy(bookCopies.get(random.nextInt(bookCopies.size())));
            borrowRepository.save(borrow);
        }
    }
    public void seedBookCopy(int generatedBookCopy) {
        List<Book> books = bookRepository.findAll();
        for (int i = 0; i < generatedBookCopy; i++){
            BookCopy bookCopy = new BookCopy();
            bookCopy.setBook(books.get(random.nextInt(books.size())));
            bookCopyRepository.save(bookCopy);
        }
    }

    public void seedBooks(int generatedBooks) {
        for (int i = 0; i < generatedBooks; i++) {
            Book book = new Book();
            bookRepository.save(book);
        }
    }

    public void seedUsers(int generatedUsers) {
        for (int i = 0; i < generatedUsers; i++) {
            User user = new User();
            userRepository.save(user);
        }
    }
    @Override
    public void run(ApplicationArguments args) throws Exception {
        seedUsers(50);
        seedBooks(50);
        seedBookCopy(50);
        seedBorrows(50);
    }
}
