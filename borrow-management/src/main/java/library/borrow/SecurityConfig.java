package library.borrow;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import library.borrow.data.model.auth.Scopes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;


@Configuration
@SecurityScheme(
        name = "Bearer Authentication",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        scheme = "bearer"
)
public class SecurityConfig {

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(x -> x
                        .requestMatchers(HttpMethod.PUT).hasAuthority(Scopes.LIBRARIAN)
                        .requestMatchers(HttpMethod.POST).hasAnyAuthority(Scopes.LIBRARIAN, Scopes.MEMBER)
                        .requestMatchers(HttpMethod.DELETE).hasAuthority(Scopes.LIBRARIAN)
                        .requestMatchers(HttpMethod.GET,  "/api/borrow/*").hasAuthority(Scopes.LIBRARIAN)
                        .anyRequest().permitAll()
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()))
        ;
        return http.build();
    }

}
