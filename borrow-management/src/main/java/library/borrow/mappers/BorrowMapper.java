package library.borrow.mappers;

import library.borrow.api.borrow.BorrowDTO;
import library.borrow.data.model.Borrow;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface BorrowMapper {

    BorrowMapper INSTANCE = Mappers.getMapper(BorrowMapper.class);

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "bookCopy.id", target = "bookCopyId")
    BorrowDTO borrowToDTO(Borrow borrow);

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "bookCopy.id", target = "bookCopyId")
    List<BorrowDTO> borrowsToDTOs(List<Borrow> borrows);
}