package library.borrow.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import library.borrow.api.borrow.BorrowDTO;
import library.borrow.api.borrow.CreateBorrowRequestDTO;
import library.borrow.api.borrow.UpdateBorrowRequestDTO;
import library.borrow.data.model.Borrow;
import library.borrow.facade.BorrowFacade;
import library.borrow.mappers.BorrowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/borrow")
public class BorrowController {

    @Autowired
    private BorrowFacade borrowFacade;

    @PostMapping
    @Operation(
            operationId = "createBorrow",
            summary = "Create a new borrow",
            tags = { "BorrowController" },
            security = { @SecurityRequirement(name = "Bearer Authentication") },
            responses = {
                    @ApiResponse(responseCode = "201", description = "Created", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = BorrowDTO.class))
                    }),
                    @ApiResponse(responseCode = "400", description = "Bad request")
            }
    )
    public ResponseEntity<BorrowDTO> createBorrow(@RequestBody CreateBorrowRequestDTO createBorrowRequestDTO) {

        Borrow newBorrow = borrowFacade.createBorrow(createBorrowRequestDTO);
        BorrowDTO newBorrowDTO = BorrowMapper.INSTANCE.borrowToDTO(newBorrow);

        return new ResponseEntity<>(newBorrowDTO, HttpStatus.CREATED);
    }

    @PutMapping("/{borrowId}")
    @Operation(
            operationId = "updateBorrow",
            summary = "Update an existing borrow",
            tags = { "BorrowController" },
            security = { @SecurityRequirement(name = "Bearer Authentication") },
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = BorrowDTO.class))
                    }),
                    @ApiResponse(responseCode = "400", description = "Bad request")
            }
    )
    public ResponseEntity<BorrowDTO> updateBorrow(@PathVariable Long borrowId, @RequestBody UpdateBorrowRequestDTO updateBorrowRequestDTO) {

        Borrow updatedBorrow = borrowFacade.updateBorrow(borrowId,updateBorrowRequestDTO);
        BorrowDTO updatedBorrowDTO = BorrowMapper.INSTANCE.borrowToDTO(updatedBorrow);
        return new ResponseEntity<>(updatedBorrowDTO, HttpStatus.OK);
    }


    @GetMapping("/{borrowId}")
    @Operation(
            operationId = "getBorrowById",
            summary = "Get borrow by ID",
            tags = { "BorrowController" },
            security = { @SecurityRequirement(name = "Bearer Authentication") },
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = BorrowDTO.class))
                    }),
                    @ApiResponse(responseCode = "404", description = "Borrow not found")
            }
    )
    public ResponseEntity<BorrowDTO> getBorrowById(@PathVariable Long borrowId) {
        Borrow borrow = borrowFacade.getBorrowById(borrowId);
        if (borrow != null) {
            BorrowDTO borrowDTO = BorrowMapper.INSTANCE.borrowToDTO(borrow);
            return new ResponseEntity<>(borrowDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{borrowId}")
    @Operation(
            operationId = "deleteBorrow",
            summary = "Delete borrow by ID",
            tags = { "BorrowController" },
            security = { @SecurityRequirement(name = "Bearer Authentication") },
            responses = {
                    @ApiResponse(responseCode = "204", description = "No content"),
                    @ApiResponse(responseCode = "404", description = "Borrow not found")
            }
    )
    public ResponseEntity<Void> deleteBorrow(@PathVariable Long borrowId) {
        boolean deleted = borrowFacade.deleteBorrow(borrowId);
        if (deleted) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/all")
    @Operation(
            operationId = "getAllBorrows",
            summary = "Get all borrows",
            tags = { "BorrowController" },
            security = { @SecurityRequirement(name = "Bearer Authentication") },
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = BorrowDTO.class))
                    }),
                    @ApiResponse(responseCode = "204", description = "No content")
            }
    )
    public ResponseEntity<List<BorrowDTO>> getAllBorrows() {
        List<Borrow> borrows = borrowFacade.getAllBorrows();
        if (!borrows.isEmpty()) {
            List<BorrowDTO> borrowDTOs = BorrowMapper.INSTANCE.borrowsToDTOs(borrows);
            return ResponseEntity.ok(borrowDTOs);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @PostMapping("/return/{borrowId}")
    @Operation(
            operationId = "returnBook",
            summary = "Return a borrowed book",
            tags = { "BorrowController" },
            security = { @SecurityRequirement(name = "Bearer Authentication") },
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "404", description = "Not Found")
            }
    )
    public ResponseEntity<Void> returnBook(@PathVariable  Long borrowId) {
        boolean returned = borrowFacade.returnBook(borrowId);
        if (returned) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @GetMapping("/overdue")
    @Operation(
            operationId = "getOverdueBorrows",
            summary = "Get overdue borrows",
            tags = { "BorrowController" },
            security = { @SecurityRequirement(name = "Bearer Authentication") },
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = BorrowDTO.class))
                    }),
                    @ApiResponse(responseCode = "204", description = "No content")
            }
    )
    public ResponseEntity<List<BorrowDTO>> getOverdueBorrows() {
        List<Borrow> overdueBorrows = borrowFacade.getOverdueBorrows();
        if (!overdueBorrows.isEmpty()) {
            List<BorrowDTO> overdueBorrowDTOs = BorrowMapper.INSTANCE.borrowsToDTOs(overdueBorrows);
            return ResponseEntity.ok(overdueBorrowDTOs);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

}
