package library.borrow.Integration;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import library.borrow.api.borrow.BorrowDTO;
import library.borrow.api.borrow.CreateBorrowRequestDTO;
import library.borrow.api.borrow.UpdateBorrowRequestDTO;
import library.borrow.data.model.Book;
import library.borrow.data.model.BookCopy;
import library.borrow.data.model.Borrow;
import library.borrow.data.model.User;
import library.borrow.data.repository.BookCopyRepository;
import library.borrow.data.repository.BookRepository;
import library.borrow.data.repository.BorrowRepository;
import library.borrow.data.model.auth.Scopes;
import library.borrow.data.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;


import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@ActiveProfiles("test")
class BorrowIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BookCopyRepository bookCopyRepository;

    @Autowired
    private BorrowRepository borrowRepository;
    @Autowired
    private ObjectMapper objectMapper;
    @BeforeEach
    void setup(){
        borrowRepository.deleteAll();
    }

    private RequestPostProcessor mockLibrarian() {
        return SecurityMockMvcRequestPostProcessors.jwt().authorities(new SimpleGrantedAuthority(Scopes.LIBRARIAN));
    }
    @Test
    void testGetBorrowById() throws Exception {
        Borrow borrow = new Borrow();
        borrow.setBorrowDate(new Date());
        borrow.setReturnDate(new Date());
        borrow.setPrice(BigDecimal.TEN);
        borrow.setReturnLimitDays(7);
        borrow.setFinePerDay(BigDecimal.ONE);
        borrow.setReturned(false);
        borrowRepository.save(borrow);
        long id = borrow.getId();

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/borrow/{id}",id)
                        .with(mockLibrarian()))
                .andExpect(status().isOk())
                .andReturn();


        BorrowDTO responseDTO = objectMapper.readValue(result.getResponse().getContentAsString(), BorrowDTO.class);


        assertEquals(borrow.getId(), responseDTO.getId());
        assertEquals(borrow.getBorrowDate(), responseDTO.getBorrowDate());
        assertEquals(borrow.getReturnDate(), responseDTO.getReturnDate());
        double first = borrow.getPrice().doubleValue();
        double second = responseDTO.getPrice().doubleValue();
        assertEquals(first, second);
        assertEquals(borrow.getReturnLimitDays(), responseDTO.getReturnLimitDays());
        first = borrow.getFinePerDay().doubleValue();
        second = responseDTO.getFinePerDay().doubleValue();
        assertEquals(first, second);
        assertEquals(borrow.isReturned(), responseDTO.isReturned());
    }
    @Test

    void testGetAllBorrows() throws Exception {
        Borrow borrow1 = new Borrow();
        Borrow borrow2 = new Borrow();
        borrowRepository.saveAll(List.of(borrow1, borrow2));

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/borrow/all")
                        .with(mockLibrarian()))
                .andExpect(status().isOk())
                .andReturn();

        List<BorrowDTO> responseDTOs = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
        });

        assertEquals(2, responseDTOs.size());
    }
    @Test
    void testCreateBorrow() throws Exception {
        User user = new User();
        BookCopy bookCopy = new BookCopy();
        bookCopy.setId(1L);
        bookCopyRepository.save(bookCopy);
        userRepository.save(user);
        CreateBorrowRequestDTO requestDTO = new CreateBorrowRequestDTO(1L,1L,BigDecimal.TEN,1,BigDecimal.TEN);
        String requestBody = objectMapper.writeValueAsString(requestDTO);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/borrow")
                        .with(mockLibrarian())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isCreated())
                .andReturn();

        BorrowDTO responseDTO = objectMapper.readValue(result.getResponse().getContentAsString(), BorrowDTO.class);

        Borrow createdBorrow = borrowRepository.findById(responseDTO.getId()).orElse(null);

        assert createdBorrow != null;
        assertEquals(requestDTO.getReturnLimitDays(), createdBorrow.getReturnLimitDays());
    }
    @Test
    void testUpdateBorrow() throws Exception {
        Borrow existingBorrow = new Borrow();
        existingBorrow.setBorrowDate(new Date());
        existingBorrow.setReturnDate(new Date());
        existingBorrow.setPrice(BigDecimal.TEN);
        existingBorrow.setReturnLimitDays(7);
        existingBorrow.setFinePerDay(BigDecimal.ONE);
        existingBorrow.setReturned(false);
        borrowRepository.save(existingBorrow);

        UpdateBorrowRequestDTO updateRequestDTO = new UpdateBorrowRequestDTO();
        updateRequestDTO.setBorrowDate(new Date());
        updateRequestDTO.setPrice(BigDecimal.TEN);
        updateRequestDTO.setReturnLimitDays(17);
        updateRequestDTO.setFinePerDay(BigDecimal.ONE);
        String requestBody = objectMapper.writeValueAsString(updateRequestDTO);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.put("/api/borrow/{borrowId}", existingBorrow.getId())
                        .with(mockLibrarian())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andReturn();

        BorrowDTO updatedBorrowDTO = objectMapper.readValue(result.getResponse().getContentAsString(), BorrowDTO.class);


        assertEquals(existingBorrow.getId(), updatedBorrowDTO.getId());
        assertEquals(17, updatedBorrowDTO.getReturnLimitDays());
    }
    @Test
    void testDeleteBorrow() throws Exception {
        Borrow borrow = new Borrow();
        borrowRepository.save(borrow);

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/borrow/{borrowId}", borrow.getId())
                        .with(mockLibrarian())
                        .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isNoContent());

        boolean existsAfterDeletion = borrowRepository.existsById(borrow.getId());
        assertFalse(existsAfterDeletion);
    }

    @Test
    void testReturnBook() throws Exception {
        Borrow borrow = new Borrow();
        borrowRepository.save(borrow);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/borrow/return/{borrowId}", borrow.getId())
                        .with(mockLibrarian())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Borrow returnedBorrow = borrowRepository.findById(borrow.getId()).orElse(null);
        assert returnedBorrow != null;
        assertTrue(returnedBorrow.isReturned());
    }
    @Test
    void testGetOverdueBorrows() throws Exception {

        Borrow notoverdueBorrow = new Borrow();
        notoverdueBorrow.setReturnLimitDays(15);
        borrowRepository.save(notoverdueBorrow);

        Borrow overdueBorrow = new Borrow();
        overdueBorrow.setReturnLimitDays(-15);
        borrowRepository.save(overdueBorrow);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/borrow/overdue")
                        .with(mockLibrarian()))
                .andExpect(status().isOk())
                .andReturn();


        List<BorrowDTO> responseDTOs = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
        });

        assertEquals(1, responseDTOs.size());
        assertEquals(overdueBorrow.getReturnLimitDays(), responseDTOs.getFirst().getReturnLimitDays());
    }




}
