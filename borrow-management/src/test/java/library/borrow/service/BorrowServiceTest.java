package library.borrow.service;

import library.borrow.api.borrow.CreateBorrowRequestDTO;
import library.borrow.api.borrow.UpdateBorrowRequestDTO;
import library.borrow.data.model.BookCopy;
import library.borrow.data.model.Borrow;
import library.borrow.data.model.User;
import library.borrow.data.repository.BookCopyRepository;
import library.borrow.data.repository.BorrowRepository;
import library.borrow.data.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BorrowServiceTest {

	@Mock
	private BorrowRepository borrowRepository;

	@Mock
	private BookCopyRepository bookCopyRepository;

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private BorrowService borrowService;

	@Test
	void createBorrow_happyPath() {
		CreateBorrowRequestDTO createRequest = new CreateBorrowRequestDTO();
		createRequest.setUserId(1L); // Assuming setter methods exist
		createRequest.setBookId(1L);
		createRequest.setReturnLimitDays(14);
		createRequest.setPrice(new BigDecimal("10.00"));
		createRequest.setFinePerDay(new BigDecimal("0.50"));

		User user = new User();
		BookCopy bookCopy = new BookCopy();
		Borrow savedBorrow = new Borrow();

		when(userRepository.findById(createRequest.getUserId())).thenReturn(Optional.of(user));
		when(bookCopyRepository.findById(createRequest.getCopyBookId())).thenReturn(Optional.of(bookCopy));
		when(borrowRepository.save(any(Borrow.class))).thenReturn(savedBorrow);

		Borrow result = borrowService.createBorrow(createRequest);

		assertNotNull(result);
		verify(userRepository).findById(createRequest.getUserId());
		verify(bookCopyRepository).findById(createRequest.getCopyBookId());
		verify(borrowRepository).save(any(Borrow.class));
	}

	@Test
	void updateBorrow_onExistingBorrow_shouldUpdateBorrow() {

		final Long existingBorrowId = 1L;

		UpdateBorrowRequestDTO updateRequest = new UpdateBorrowRequestDTO();
		updateRequest.setPrice(new BigDecimal("10.00"));
		updateRequest.setReturnLimitDays(30);
		updateRequest.setFinePerDay(new BigDecimal("0.50"));

		Borrow existingBorrow = new Borrow();
		existingBorrow.setId(existingBorrowId);
		existingBorrow.setPrice(new BigDecimal("5.00"));
		existingBorrow.setReturnLimitDays(15);
		existingBorrow.setFinePerDay(new BigDecimal("0.25"));

		when(borrowRepository.findById(existingBorrowId)).thenReturn(Optional.of(existingBorrow));
		when(borrowRepository.save(any(Borrow.class))).thenAnswer(invocation -> invocation.getArgument(0));
		Borrow updatedBorrow = borrowService.updateBorrow(existingBorrowId, updateRequest);

		assertNotNull(updatedBorrow);
		assertEquals(existingBorrowId, updatedBorrow.getId());
		assertEquals(updateRequest.getPrice(), updatedBorrow.getPrice());
		assertEquals(updateRequest.getReturnLimitDays(), updatedBorrow.getReturnLimitDays());
		assertEquals(updateRequest.getFinePerDay(), updatedBorrow.getFinePerDay());
	}

	@Test
	void updateBorrow_onNotExistBorrow_shouldRaiseIllegalArgumentException() {
		final long notExistingBorrowId = 1L;

		when(borrowRepository.findById(notExistingBorrowId)).thenReturn(Optional.empty());

		UpdateBorrowRequestDTO requestDto = new UpdateBorrowRequestDTO();

		assertThrows(IllegalArgumentException.class,
				()->{
					borrowService.updateBorrow(notExistingBorrowId, requestDto);
				});
	}

	@Test
	void getBorrowById_onNotExistsBorrow_shouldReturnNull() {
		final long notExistingBorrowId = 1L;

		when(borrowRepository.findById(notExistingBorrowId)).thenReturn(Optional.empty());

		assertNull(borrowService.getBorrowById(notExistingBorrowId));
	}

	@Test
	void getBorrowById_onExistingBorrow_shouldReturnBorrow() {
		final long existingBorrowId = 1L;
		Borrow borrow = new Borrow();

		when(borrowRepository.findById(existingBorrowId)).thenReturn(Optional.of(borrow));

		assertEquals(borrowService.getBorrowById(existingBorrowId), borrow);
	}

	@Test
	void deleteBorrow_onNotExistsBorrow_shouldReturnFalse() {
		final long notExistingBorrowId = 1L;

		when(borrowRepository.findById(notExistingBorrowId)).thenReturn(Optional.empty());
		boolean result = borrowService.deleteBorrow(notExistingBorrowId);

		assertFalse(result);
	}

	@Test
	void deleteBorrow_onExistingBorrow_shouldReturnTrue()
	{
		final long existingBorrowId = 1L;
		Borrow borrow = new Borrow();

		when(borrowRepository.findById(existingBorrowId)).thenReturn(Optional.of(borrow));
		boolean result = borrowService.deleteBorrow(existingBorrowId);

		assertTrue(result);
	}
}