package library.borrow.rest;

import library.borrow.api.borrow.CreateBorrowRequestDTO;
import library.borrow.data.model.Borrow;
import library.borrow.data.model.auth.Scopes;
import library.borrow.facade.BorrowFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(BorrowController.class)
public class BorrowControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private BorrowFacade borrowFacade;

	private RequestPostProcessor mockLibrarian() {
		return SecurityMockMvcRequestPostProcessors.jwt().authorities(new SimpleGrantedAuthority(Scopes.LIBRARIAN));
	}
	@Test
	public void getBorrowById_returnsNotFound_whenNotPresent() throws Exception {
		Long borrowId = 2L;

		given(borrowFacade.getBorrowById(borrowId)).willReturn(null);

		mockMvc.perform(get("/api/borrow/{borrowId}", borrowId)
						.with(mockLibrarian())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void getBorrowById_returnsBorrowDTO_whenFound() throws Exception {
		Long borrowId = 5L;
		Borrow borrow = new Borrow();
		borrow.setId(borrowId);

		given(borrowFacade.getBorrowById(borrowId)).willReturn(borrow);

		mockMvc.perform(get("/api/borrow/{borrowId}", borrowId)
						.with(mockLibrarian())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(borrowId));
	}

	@Test
	void deleteBorrow_returnsNoContent_whenDeletionIsSuccessful() throws Exception {
		Long borrowId = 1L;
		given(borrowFacade.deleteBorrow(borrowId)).willReturn(true);

		mockMvc.perform(delete("/api/borrow/{borrowId}", borrowId)
						.with(mockLibrarian())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNoContent());
	}

	@Test
	void deleteBorrow_returnsNotFound_whenBorrowDoesNotExist() throws Exception {
		Long borrowId = 2L;
		given(borrowFacade.deleteBorrow(borrowId)).willReturn(false);

		mockMvc.perform(delete("/api/borrow/{borrowId}", borrowId)
						.with(mockLibrarian())
			.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void getAllBorrows_returnsBorrowDTOList_whenListIsNotEmpty() throws Exception {
		List<Borrow> borrows = new ArrayList<>();
		Borrow borrow = new Borrow();
		borrows.add(borrow);

		given(borrowFacade.getAllBorrows()).willReturn(borrows);

		mockMvc.perform(get("/api/borrow/all")
						.contentType(MediaType.APPLICATION_JSON)
						.with(mockLibrarian()))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$[0]").isNotEmpty());
	}

	@Test
	public void createBorrow_returnsCreatedStatus_whenSuccessful() throws Exception {
		Borrow newBorrow = new Borrow();

		given(borrowFacade.createBorrow(any(CreateBorrowRequestDTO.class))).willReturn(newBorrow);

		String requestBody = "{\"userId\":1}";

		mockMvc.perform(post("/api/borrow")
						.with(mockLibrarian())
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestBody))
				.andExpect(status().isCreated());
	}

	@Test
	public void updateBorrow_successful() throws Exception {
		Long borrowId = 1L;
		String requestBodyUpdate = "{\"price\":25.0,\"returnLimitDays\":30,\"finePerDay\":1.5}";

		mockMvc.perform(put("/api/borrow/{borrowId}", borrowId)
						.with(mockLibrarian())
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestBodyUpdate))
				.andExpect(status().isOk());
	}
}