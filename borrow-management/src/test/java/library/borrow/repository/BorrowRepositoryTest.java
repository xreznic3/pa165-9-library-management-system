package library.borrow.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;


import java.util.Date;
import java.util.List;

import library.borrow.api.user.UserRole;
import library.borrow.data.model.Borrow;
import library.borrow.data.model.User;
import library.borrow.data.repository.BorrowRepository;
import library.borrow.data.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
public class BorrowRepositoryTest {

    @Autowired
    private BorrowRepository borrowRepository;
    @Autowired
    private UserRepository userRepository;

    private User getUser() {
        User user = new User();
        user.setId(1L);
        return user;
    }

    @Test
    public void testGetOverdueBorrows() {
        Borrow borrow1 = new Borrow();
        borrow1.setReturnLimitDays(-5);
        borrow1.setReturned(false);
        Borrow borrow2 = new Borrow();

        borrowRepository.save(borrow1);
        borrowRepository.save(borrow2);

        List<Borrow> overdueBorrows = borrowRepository.getOverdueBorrows(new Date());
        assertEquals(1, overdueBorrows.size());
        assertEquals(-5, overdueBorrows.getFirst().getReturnLimitDays());
    }

    @Test
    public void testFindOverdueBorrowsWithoutFines() {
        Long userId = 1L;
        Borrow borrow1 = new Borrow();
        User user = getUser();
        userRepository.save((user));
        borrow1.setUser(user);
        borrow1.setReturnLimitDays(-5);
        borrow1.setReturned(false);
        Borrow borrow2 = new Borrow();

        borrowRepository.save(borrow1);
        borrowRepository.save(borrow2);

        List<Borrow> overdueBorrows = borrowRepository.findOverdueBorrowsWithoutFines(userId);

        assertEquals(1, overdueBorrows.size());
    }
}
