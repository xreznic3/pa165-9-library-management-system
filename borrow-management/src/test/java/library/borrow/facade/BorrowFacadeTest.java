package library.borrow.facade;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import library.borrow.api.borrow.CreateBorrowRequestDTO;
import library.borrow.data.model.Borrow;
import library.borrow.data.repository.BorrowRepository;
import library.borrow.service.BorrowService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class BorrowFacadeTest {


    @Mock
    private BorrowService borrowService;
    @Mock
    private BorrowRepository borrowRepository;
    @InjectMocks
    private BorrowFacade borrowFacade;


    @Test
    public void testGetBorrowById_ReturnsNull_WhenNotPresent() {
        Long borrowId = 1L;

        when(borrowService.getBorrowById(borrowId)).thenReturn(null);

        assertNull(borrowFacade.getBorrowById(borrowId));
    }

    @Test
    public void testGetBorrowById_ReturnsBorrow_WhenFound() {
        Long borrowId = 5L;
        Borrow borrow = new Borrow();
        borrow.setId(borrowId);

        when(borrowService.getBorrowById(borrowId)).thenReturn(borrow);

        assertEquals(borrow, borrowFacade.getBorrowById(borrowId));
    }

    @Test
    public void testDeleteBorrow_ReturnsTrue_WhenDeletionIsSuccessful() {
        Long borrowId = 1L;
        when(borrowService.deleteBorrow(borrowId)).thenReturn(true);

        assertTrue(borrowFacade.deleteBorrow(borrowId));
    }

    @Test
    public void testDeleteBorrow_ReturnsFalse_WhenBorrowDoesNotExist() {
        Long borrowId = 2L;
        when(borrowService.deleteBorrow(borrowId)).thenReturn(false);

        assertFalse(borrowFacade.deleteBorrow(borrowId));
    }

    @Test
    public void testGetAllBorrows_ReturnsBorrowList_WhenListIsNotEmpty() {
        List<Borrow> borrows = new ArrayList<>();
        Borrow borrow = new Borrow();
        borrows.add(borrow);

        when(borrowRepository.findAll()).thenReturn(borrows);

        assertEquals(borrows, borrowFacade.getAllBorrows());
    }

    @Test
    public void testCreateBorrow_ReturnsBorrow_WhenSuccessful() {
        CreateBorrowRequestDTO requestDTO = new CreateBorrowRequestDTO();
        Borrow newBorrow = new Borrow();

        when(borrowService.createBorrow(requestDTO)).thenReturn(newBorrow);

        assertEquals(newBorrow, borrowFacade.createBorrow(requestDTO));
    }


}
